# This extremely stupid script is checked in for if we need to run cargo update again before
# substrate is out of prerelease. To use it, update the `rc4` and `rc3` strings as needed,
# run the script, and then run the output script over and over again until it has no errors. This
# must be done because deps must be updated from the leaves up.
cargo tree | grep rc4 \
    | sed 's/^\W*/-p /' | sed 's/ v/ --precise /' | sed 's/ (.*//' | sed 's/rc4/rc3/' \
    | sort -u | xargs -i echo cargo update {} > runme.sh
