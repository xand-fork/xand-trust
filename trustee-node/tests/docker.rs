use bollard::{
    container::{Config, CreateContainerOptions, LogOutput, LogsOptions},
    errors::Error,
    models::{HostConfig, Mount, MountTypeEnum},
    Docker,
};
use futures::StreamExt;
use std::{
    fs,
    fs::File,
    io::{prelude::*, BufReader, Write},
    path::{Path, PathBuf},
};
use tempdir::TempDir;
use trustee_node::async_adapter::{
    adapters::tokio_adapter::TokioAdapter, ports::future_resolver::FutureResolver,
};
use uuid::Uuid;

const TRUSTEE_DOCKER_IMAGE: &str = "gcr.io/xand-dev/trust:latest";
const TRUSTEE_CONTAINER_NAME: &str = "trustee-integ";
const TIMESTAMP_FILENAME: &str = "faketime.rc";
const TRUSTEE_ENV_VARS: [&str; 3] = [
    "FAKETIME_TIMESTAMP_FILE=/etc/xand/trustee-node/faketime.rc",
    "LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketimeMT.so.1",
    "FAKETIME_NO_CACHE=1",
];

const TRUSTEE_ENTRYPOINT: [&str; 3] = [
    "/usr/bin/xand/trustee-node",
    "/etc/xand/trustee-node",
    "--allow-create-db",
];
const DOCKER_NETWORK_MODE: &str = "host";
const INTERNAL_INTEG_CONFIG_FOLDER: &str = "/etc/xand/trustee-node";

const INTERNAL_TRACING_FOLDER: &str = "/usr/bin/xand/traces";

#[derive(Debug)]
pub struct TrusteeContainer {
    container_name: String,
    traces_folder: TempDir,
}

impl Default for TrusteeContainer {
    fn default() -> Self {
        Self::new()
    }
}

impl TrusteeContainer {
    pub fn new() -> Self {
        Self::new_with_system_time_offset(HoursOffset::new(0))
    }

    fn set_timestamp_config_file(config_folder: &str, offset: HoursOffset) {
        let mut time_file_path = std::path::PathBuf::new();
        time_file_path.push(config_folder);
        time_file_path.push(TIMESTAMP_FILENAME);

        let mut time_file = std::fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(time_file_path)
            .unwrap();
        time_file
            .write_all(offset.libfaketime_format().as_bytes())
            .unwrap();
        time_file.flush().unwrap();
    }

    pub fn new_with_system_time_offset(initial_offset: HoursOffset) -> Self {
        let external_integ_config_folder = Self::integ_test_config_mount_source();
        Self::set_timestamp_config_file(&external_integ_config_folder, initial_offset);

        let temp_dir = TempDir::new_in(".", "trust-integ-temp").unwrap();
        let temp_traces_folder = temp_dir.path().as_os_str().to_str().unwrap().to_string();
        println!("Storing trace logs to temp folder: {}", &temp_traces_folder);

        let trustee_config = Config {
            image: Some(TRUSTEE_DOCKER_IMAGE),
            env: Some(TRUSTEE_ENV_VARS.to_vec()),
            entrypoint: Some(TRUSTEE_ENTRYPOINT.to_vec()),
            host_config: Some(HostConfig {
                network_mode: Some(DOCKER_NETWORK_MODE.to_string()),
                mounts: Some(vec![
                    Mount {
                        source: Some(external_integ_config_folder),
                        target: Some(INTERNAL_INTEG_CONFIG_FOLDER.to_string()),
                        typ: Some(MountTypeEnum::BIND),
                        ..Default::default()
                    },
                    Mount {
                        source: Some(temp_traces_folder),
                        target: Some(INTERNAL_TRACING_FOLDER.to_string()),
                        typ: Some(MountTypeEnum::BIND),
                        ..Default::default()
                    },
                ]),
                auto_remove: Some(true),
                ..Default::default()
            }),
            ..Default::default()
        };

        let container_name = format!("{}-{}", TRUSTEE_CONTAINER_NAME, Uuid::new_v4());
        let options = CreateContainerOptions {
            name: container_name.as_str(),
        };

        println!("Trust node configuration:\n{:?}", &trustee_config);

        let rt = TokioAdapter::get().expect("Could not build tokio handle");
        let docker = Self::create_docker_connection(&rt);

        let id = match rt
            .block(docker.create_container::<&str, &str>(Some(options), trustee_config))
        {
            Err(error) => {
                panic!("Failed to start container. Make sure integ tests are running single threaded; one at a time. error: {:?}", error);
            }
            Ok(create_result) => create_result.id,
        };

        rt.block(async {
            docker.start_container::<String>(&id, None).await.unwrap();

            tokio::spawn(async move {
                let log_stream = &mut docker.logs(
                    &id,
                    Some(LogsOptions {
                        follow: true,
                        stdout: true,
                        stderr: true,
                        tail: "all".to_string(),
                        ..Default::default()
                    }),
                );

                while let Some(log) = log_stream.next().await {
                    Self::print_docker_logs(log)
                }
            });
        });

        Self {
            container_name,
            traces_folder: temp_dir,
        }
    }

    fn print_docker_logs(log: Result<LogOutput, Error>) {
        match log {
            // Unfortunately, bollard::Error doesn't impl `Display` while the Debug output for
            // LogOutput loses all formatting (except for a trailing `\n`). We need to print each
            // differently for best readability.
            Ok(l) => print!("{}", l),
            Err(e) => println!("{:#?}", e),
        }
    }

    fn create_docker_connection(rt: &TokioAdapter) -> Docker {
        // First try to connect via TCP 2375
        if let Ok(docker) = Docker::connect_with_http_defaults() {
            // test the connection and return if ok
            if let Ok(_info) = rt.block(docker.info()) {
                return docker;
            }
        }

        // sometimes dockerd isn't listening on TCP 2375, so try local socket
        let docker = Docker::connect_with_socket_defaults().unwrap();
        // test the connection
        let _info = rt.block(docker.info()).unwrap();
        docker
    }

    fn integ_test_config_mount_source() -> String {
        let integration_config_folder = PathBuf::from("../integ-test-config");
        let absolute_integ_config_folder = fs::canonicalize(integration_config_folder).unwrap();
        absolute_integ_config_folder
            .into_os_string()
            .into_string()
            .unwrap()
    }

    pub fn set_system_time_offset(&self, new_offset: HoursOffset) {
        let integ_config_folder = Self::integ_test_config_mount_source();
        Self::set_timestamp_config_file(&integ_config_folder, new_offset);
    }

    pub fn get_text_traces(&self) -> Vec<String> {
        let text_tracing_file_path = &self.traces_folder.path().join("tracedata.log");
        Self::read_file_lines(text_tracing_file_path)
    }

    pub fn get_open_telemetry_traces(&self) -> Vec<String> {
        let open_telemetry_tracing_file_path = &self.traces_folder.path().join("opentelemetry.log");
        Self::read_file_lines(open_telemetry_tracing_file_path)
    }

    fn read_file_lines(file: &Path) -> Vec<String> {
        let file = File::open(file).expect("Unable to open text tracing file");
        let buf = BufReader::new(file);
        buf.lines()
            .map(|l| l.expect("Could not parse line"))
            .collect()
    }
}

impl Drop for TrusteeContainer {
    fn drop(&mut self) {
        let rt = TokioAdapter::get().expect("Could not build tokio handle");
        let docker = Self::create_docker_connection(&rt);
        if let Err(error) = rt.block(docker.stop_container(&self.container_name, None)) {
            println!("Error while stopping trustee container: {:?}", error);
        }
    }
}

#[derive(Debug)]
pub struct HoursOffset {
    hours_offset: i32,
}

impl HoursOffset {
    pub fn new(hours_offset: i32) -> Self {
        Self { hours_offset }
    }

    pub fn libfaketime_format(&self) -> String {
        match self.hours_offset {
            i32::MIN..=-1 => format!("{}h", self.hours_offset),
            0 => "".to_string(),
            1..=i32::MAX => format!("+{}h", self.hours_offset),
        }
    }
}
