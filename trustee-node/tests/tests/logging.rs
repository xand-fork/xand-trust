use once_cell::sync::Lazy;

// Initializes the logger only on first access.
static LOGGER: Lazy<()> = Lazy::new(|| {
    let subscriber = tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .finish();
    match tracing::subscriber::set_global_default(subscriber) {
        Ok(_) => {
            println!("Logging initialized")
        }
        Err(e) => println!("Failed to initialize logging: {}", e),
    }
});

pub fn init_stdout_logging() {
    Lazy::force(&LOGGER);
}
