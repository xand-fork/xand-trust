pub use member_actor::*;

mod member_actor {
    use crate::tests::actors::data::{Amount, Block, XandTxnReceipt};
    use xand_api_client::CorrelationId;

    /// Tests use this trait to issue activity as Members
    pub trait MemberActor {
        fn full_create_claims_flow(&self, _amount: Amount) -> XandTxnReceipt;
        fn full_redeem_claims_flow(&self, _amount: Amount) -> XandTxnReceipt;
        fn get_finalized_block_num(&self) -> Block;
        fn get_xand_balance(&self) -> Amount;
        fn get_bank_balance(&self) -> Amount;
        fn transfer_funds_for_create(&self, id: &CorrelationId, _amount: Amount);
        fn submit_create_request(&self, id: &CorrelationId, _amount: Amount) -> XandTxnReceipt;
    }
}

pub mod bank_actor {
    use crate::tests::actors::data::{Amount, BankAccount};

    /// Tests and adapters use this trait to issue activity against a particular BankAccount
    pub trait BankAccountActor {
        fn get_balance(&self) -> Amount;
        fn transfer_to(&self, amount: Amount, metadata: String, recipient: BankAccount);
    }
}

pub mod config_provider {
    use xand_network_generator::contracts::network::metadata::xand_entity_set_summary::MemberMetadata;

    /// Tests and adapters use this trait to fetch data about how the test environment (entities, addresses, urls, etc)
    pub trait ConfigProvider {
        fn mcb_url(&self) -> String;
        fn member_metadata_at_index(&self, index: usize) -> MemberMetadata;
        fn trust_mcb_account(&self) -> String;
    }
}
