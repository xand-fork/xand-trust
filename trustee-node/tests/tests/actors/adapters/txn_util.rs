use crate::tests::actors::data::XandTxnReceipt;
use xand_api_client::{
    errors::XandApiClientError, CreateRequestCompletion, RedeemRequestCompletion, Transaction,
    XandTransaction,
};

pub fn is_fulfilled(
    receipt: &XandTxnReceipt,
    details: Result<Transaction, XandApiClientError>,
) -> Option<Fulfilled> {
    match details {
        Ok(txn) => get_fulfillment_reason(txn),
        Err(e) => match e {
            XandApiClientError::NotFound { .. } => {
                tracing::info!("Transaction {:?} not yet found", &receipt);
                None
            }
            _ => panic!("Unexpected error when checking transaction status: {:?}", e),
        },
    }
}

pub fn get_fulfillment_reason(txn: Transaction) -> Option<Fulfilled> {
    match txn.txn {
        XandTransaction::CreateRequest(pending_create) => {
            match pending_create.completing_transaction {
                Some(completing_txn) => {
                    let fulfill_status = match completing_txn {
                        CreateRequestCompletion::Confirmation(_) => Fulfilled::Healthy,
                        CreateRequestCompletion::Cancellation(_) => Fulfilled::Cancelled,
                        CreateRequestCompletion::Expiration(_) => Fulfilled::Expired,
                    };
                    Some(fulfill_status)
                }
                None => None,
            }
        }
        XandTransaction::RedeemRequest(pending_redeem) => {
            match pending_redeem.completing_transaction {
                Some(completing_txn) => {
                    let fulfill_status = match completing_txn {
                        RedeemRequestCompletion::Confirmation(_) => Fulfilled::Healthy,
                        RedeemRequestCompletion::Cancellation(_) => Fulfilled::Cancelled,
                    };
                    Some(fulfill_status)
                }
                None => None,
            }
        }
        _ => panic!("Unexpected txn type returned: {:?}", txn.txn),
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum Fulfilled {
    Healthy,
    Cancelled,
    Expired,
}
