mod util;

use crate::tests::actors::{
    adapters::{
        bank_actor_impl::util::FakeSecrets, config_provider::XandEntitySetSummaryConfigProvider,
    },
    data::BankAccount,
    ports::{bank_actor::BankAccountActor, config_provider::ConfigProvider},
};
use num_traits::ToPrimitive;
use std::sync::Arc;
use trustee_node::async_adapter::{
    adapters::tokio_adapter::TokioAdapter, ports::sync_future_ext::SyncFutureExt,
};
use xand_banks::{
    banks::mcb_adapter::{
        config::{McbClientTokenRetrievalAuthConfig, McbConfig, RawAccountInfoManagerConfig},
        CloneableMcbAdapter,
    },
    models::account::TransferRequest,
    xand_money::{Money, Usd},
    BankAdapter,
};

pub struct McbActor {
    account: BankAccount,
    mcb_client: CloneableMcbAdapter,
}

impl McbActor {
    pub fn new(account: BankAccount) -> Self {
        let cfg = XandEntitySetSummaryConfigProvider::new();

        // Get bank url
        let mcb_url = cfg.mcb_url();

        // Get bank client
        let mcb_client = CloneableMcbAdapter::new(
            &McbConfig {
                url: mcb_url.parse().unwrap(),
                refresh_if_expiring_in_secs: None,
                auth: McbClientTokenRetrievalAuthConfig {
                    secret_key_username: "my-fake-metropolitan-username".to_string(),
                    secret_key_password: "my-fake-metropolitan-password".to_string(),
                },
                account_info_manager: RawAccountInfoManagerConfig {
                    secret_key_client_app_ident: "client_app_ident".to_string(),
                    secret_key_organization_id: "organization_id".to_string(),
                },
                timeout: 60,
            },
            Arc::new(FakeSecrets),
        )
        .unwrap();
        Self {
            account,
            mcb_client,
        }
    }
}

impl BankAccountActor for McbActor {
    fn get_balance(&self) -> u128 {
        // Query for balance
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        let bal = self
            .mcb_client
            .balance(&self.account.account_number())
            .block_with(&tokio_handle)
            .unwrap();

        bal.available_balance
            .into_minor_units()
            .to_u128()
            .expect("Must convert to u128")
    }

    fn transfer_to(&self, amount: u128, metadata: String, recipient: BankAccount) {
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        self.mcb_client
            .transfer(
                TransferRequest {
                    src_account_number: self.account.account_number(),
                    dst_account_number: recipient.account_number(),
                    amount: Usd::from_u64_minor_units(amount as u64)
                        .expect("Must convert Amount to Usd"),
                },
                Some(metadata),
            )
            .block_with(&tokio_handle)
            .expect("Failed submitting mcb bank transfer");
    }
}
