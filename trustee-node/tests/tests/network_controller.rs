use std::path::PathBuf;

const XAND_DOCKER_COMPOSE_REL_ROOT: &str = "../xng/generated";

pub fn compose_file_location() -> PathBuf {
    [XAND_DOCKER_COMPOSE_REL_ROOT, "docker-compose.yaml"]
        .iter()
        .collect()
}
pub fn compose_env_file_location() -> PathBuf {
    [XAND_DOCKER_COMPOSE_REL_ROOT, ".env"].iter().collect()
}
