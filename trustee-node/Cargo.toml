[package]
authors = ['Transparent Financial Systems <support@tpfs.io>']
edition = "2021"
name = "trustee-node"
publish = ["tpfs"]
version = "10.1.1"

[features]
integ_tests = []
live-bbva = []
live-sg = []

[[bin]]
name = 'trustee-node'
path = 'src/main.rs'

[[test]]
name = "trust_integ"
path = "tests/trust_integ.rs"
required-features = ["integ_tests"]

[dependencies]
async-trait = "0.1.36"
chrono = "0.4.15"
clap = "2.33.3"
config = "0.10.1"
derive_more = "0.99.9"
glob = "0.3.0"
itertools = "0.9.0"
lazy_static = "1.4.0"
log = "0.4.11"
num-traits = "0.2.12"
opentelemetry = { version = "0.16.0", features = ["serialize", "rt-tokio"] }
rolling-file = "0.1.0"
serde = "1.0.115"
serde_derive = "1.0.115"
serde_json = "1.0.57"
serde_path_to_error = "0.1.3"
snafu = "0.6.8"
thiserror = "1.0"
tokio = {version = "1.8.2", features = ["full"]}
tracing = { version = "0.1.28", features = ["log"] }
tracing-appender = "0.2"
tracing-subscriber = { version = "0.3", features = ["env-filter", "fmt", "tracing-log"] }
tracing-opentelemetry = "0.16"
url = "1.7"

# 1st party local deps
bank-transaction-repository = {version = "0.15", registry = "tpfs"}
pipesrv-emitter = {version = "0.1.0-beta", registry = "tpfs"}
opentelemetry-json = {version = "0.1.2", registry = "tpfs"}
xand-address = {version = "0.4.2", registry = "tpfs"}
xand-api-client = {version = "49.0.0", registry = "tpfs" }
xand-banks = {version = "15.1.4", registry = "tpfs"}
# Needed because xand-banks uses it in API - xand-banks should re-export. Lock to ver used in banks
xand-logging-metrics = {version = "0.1.0-beta", registry = "tpfs"}
xand-metrics = {version = "0.1.0-beta", registry = "tpfs"}
xand-metrics-prometheus = {version = "0.1.0-beta", registry = "tpfs"}
xand-secrets = {version = "0.4.2", registry = "tpfs"}
xand-secrets-local-file = {version = "0.2.0", registry = "tpfs"}
xand-utils = {version = "1.0.0", registry = "tpfs"}
# This version number (0.0.0) is managed by CI automation.
futures = "0.3.15"
trustee-node-config = {path = "../trustee-node-config/", version = "0.0.0", registry = "tpfs"}

[dependencies.uuid]
version = "1.2.2"
features = [
    "v4",                # Lets you generate random UUIDs
    "fast-rng",          # Use a faster (but still sufficiently random) RNG
    "macro-diagnostics", # Enable better diagnostics for compile-time UUIDs
]

[dev-dependencies]
assert_matches = "1.5.0"
bollard = "0.11.0"
fake_clock = "0.3.0"
galvanic-assert = "0.8.7"
insta = "1.8.0"
network-maestro = {version = "2.0.0", registry = "tpfs"}
once_cell = "1.9.0"
pseudo = "0.2"
rand = "0.8"
serde_yaml = "0.8.17"
tempfile = "3.1.0"
tempdir = "0.3.7"
tracing-assert-core = {version = "0.1.1", registry = "tpfs"}
tracing-assert-macros = {version = "0.1.1", registry = "tpfs"}
xand_network_generator = {version = "1.1.0", registry = "tpfs"}
