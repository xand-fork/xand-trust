use crate::{
    core::{CreateProcessor, TrustBankDispatcher, TxnIssuer},
    errors::TrustError,
    logging_events::LoggingEvent,
};
use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
};
use trustee_node_config::Account;
use xand_api_client::{
    BankAccountInfo, CorrelationId, CreateCancellationReason, PendingCreateRequest,
};

#[derive(PartialEq, Debug, Clone)]
enum CreateStatus {
    New,
    Completed,
    CancelRequested,
}

#[derive(Clone, Debug, PartialEq)]
struct StatusEntry {
    create_req: PendingCreateRequest,
    status: CreateStatus,
}

pub struct InMemoryCreateProcessor<'a, BD: TrustBankDispatcher, T: TxnIssuer> {
    in_progress_creates: HashMap<CorrelationId, StatusEntry>,
    dispatcher: &'a BD,
    txn_issuer: &'a T,
}

impl<'a, BD: TrustBankDispatcher, T: TxnIssuer> CreateProcessor
    for InMemoryCreateProcessor<'a, BD, T>
{
    #[tracing::instrument(skip(self))]
    fn receive_pending_creates(&mut self, create_reqs: Vec<PendingCreateRequest>) {
        tracing::info!(message = ?LoggingEvent::ReceivedPendingCreates {
            num_creates: create_reqs.len()
        });

        self.reconcile_new_pending_creates(create_reqs);
        self.process_pending_creates();
    }
}

impl<'a, BD: TrustBankDispatcher, T: TxnIssuer> InMemoryCreateProcessor<'a, BD, T> {
    pub fn new(bank_dispatcher: &'a BD, txn_issuer: &'a T) -> InMemoryCreateProcessor<'a, BD, T> {
        InMemoryCreateProcessor {
            in_progress_creates: HashMap::new(),
            dispatcher: bank_dispatcher,
            txn_issuer,
        }
    }

    fn add_new_pending_create(&mut self, id: CorrelationId, create_req: PendingCreateRequest) {
        tracing::info!(message = ?LoggingEvent::TrackingNewPendingCreate {
            id: id.clone()
        });
        self.in_progress_creates.insert(
            id,
            StatusEntry {
                create_req,
                status: CreateStatus::New,
            },
        );
    }

    /// Every time we query for pending creates, it is expected that that list represents
    /// the current legitimately outstanding creates. As a result, ones we complete will be missing
    /// in later calls - and sometimes ones the trust does *not* complete may also be missing,
    /// because they were cancelled by the user or expired automatically. Those also need to
    /// be removed from our pending map so that we do not try to fulfill them.
    #[tracing::instrument(skip(self))]
    fn reconcile_new_pending_creates(&mut self, create_reqs: Vec<PendingCreateRequest>) {
        // Go through all the creates we *do* know about and eliminate ones that the
        // chain no longer thinks exist.
        self.remove_old_pending_creates(&create_reqs);

        // Add all new creates we haven't seen before
        for create in create_reqs {
            if !self
                .in_progress_creates
                .contains_key(&create.correlation_id)
            {
                self.add_new_pending_create(create.clone().correlation_id, create)
            }
        }
    }

    fn remove_old_pending_creates(&mut self, create_reqs: &[PendingCreateRequest]) {
        self.in_progress_creates.retain(|id, _| {
            if create_reqs.iter().any(|pm| pm.correlation_id == *id) {
                true
            } else {
                tracing::info!(message = ?LoggingEvent::RemoveTrackedPendingCreate {
                id: (*id).clone()
                });
                false
            }
        });
    }

    #[tracing::instrument(skip(self))]
    fn process_pending_creates(&mut self) {
        for entry in self.in_progress_creates.values_mut() {
            process_single_pending_create(self.dispatcher, self.txn_issuer, entry).unwrap_or_else(
                |e| {
                    let event = LoggingEvent::TrustError { err: e };
                    tracing::error!(message = ?event)
                },
            );
        }
    }
}

/// If a create request is valid, attempt to complete it. If not valid, remove it from the chain.
#[tracing::instrument(skip(dispatcher, txn_issuer))]
fn process_single_pending_create(
    dispatcher: &dyn TrustBankDispatcher,
    txn_issuer: &dyn TxnIssuer,
    entry: &mut StatusEntry,
) -> Result<(), TrustError> {
    if let Some(reason) = invalid_reason(dispatcher, entry)? {
        remove_create(txn_issuer, entry, reason)?
    } else {
        attempt_to_complete_create(dispatcher, txn_issuer, entry)?;
    }
    Ok(())
}

/// Returns reason why create is not valid, if any.
fn invalid_reason(
    dispatcher: &dyn TrustBankDispatcher,
    entry: &StatusEntry,
) -> Result<Option<CreateCancellationReason>, TrustError> {
    match entry.try_into() {
        Ok(account) => {
            let bank_could_be_valid = dispatcher.bank_exists(&account)?;
            // The only qualification currently for validity is that the associated account is
            // for a valid bank.
            if bank_could_be_valid {
                Ok(None)
            } else {
                Ok(Some(CreateCancellationReason::BankNotFound))
            }
        }
        Err(_) => Ok(Some(CreateCancellationReason::InvalidData)),
    }
}

fn remove_create(
    txn_issuer: &dyn TxnIssuer,
    entry: &mut StatusEntry,
    reason: CreateCancellationReason,
) -> Result<(), TrustError> {
    match entry.status {
        CreateStatus::CancelRequested => {
            tracing::info!(message = ?LoggingEvent::AlreadyRequestedCreateCancellationNoOp{
                id: entry.create_req.correlation_id.clone()
            });
            Ok(())
        }
        _ => {
            tracing::info!(message = ?LoggingEvent::CancellingCreate{
                reason: reason.clone(),
                id: entry.create_req.correlation_id.clone()
            });
            txn_issuer.cancel_create(&entry.create_req, reason)?;
            tracing::info!(message = ?LoggingEvent::CreateCancelled{
                id: entry.create_req.correlation_id.clone()
            });
            entry.status = CreateStatus::CancelRequested;
            Ok(())
        }
    }
}

/// Looks at create request entry. If a bank transfer exists for create, a fulfillment txn
/// will be sent to the chain. If not, it will be left to be processed again later.
fn attempt_to_complete_create(
    dispatcher: &dyn TrustBankDispatcher,
    txn_issuer: &dyn TxnIssuer,
    entry: &mut StatusEntry,
) -> Result<(), TrustError> {
    tracing::info!(message = ?LoggingEvent::ProcessingPendingCreate {
        id: entry.create_req.correlation_id.clone()
    });

    if entry.status == CreateStatus::New && reserve_has_received_create_transfer(dispatcher, entry)?
    {
        tracing::info!(message = ?LoggingEvent::IssuingFulfillmentForCreate {
            id: entry.create_req.correlation_id.clone()
        });
        txn_issuer.record_create(&entry.create_req)?;
        tracing::info!(message = ?LoggingEvent::FulfillmentCompletedForCreate {
            id: entry.create_req.correlation_id.clone()
        });
        entry.status = CreateStatus::Completed;
    }
    Ok(())
}

fn reserve_has_received_create_transfer(
    dispatcher: &dyn TrustBankDispatcher,
    entry: &StatusEntry,
) -> Result<bool, TrustError> {
    let account = entry.try_into().map_err(|_| TrustError::LogicError {
        message: "already has gone through the invalid_reason function".to_string(),
    })?;
    let txn_exists = dispatcher.transfer_exists_for_create(entry.create_req.clone(), account)?;
    if txn_exists {
        tracing::info!(message = ?LoggingEvent::FoundMatchingBankTransferForCreate {
            id: entry.create_req.correlation_id.clone()
        });
    } else {
        tracing::info!(message = ?LoggingEvent::DidNotFindBankTransferForCreate {
            id: entry.create_req.correlation_id.clone()
        });
    }
    Ok(txn_exists)
}

#[derive(Debug)]
enum AccountFromEntryError {
    DataIsEncrypted,
}

impl TryFrom<&StatusEntry> for Account {
    type Error = AccountFromEntryError;

    fn try_from(entry: &StatusEntry) -> Result<Self, Self::Error> {
        match &entry.create_req.account {
            BankAccountInfo::Unencrypted(id) => Ok(Account::new(
                id.account_number.clone(),
                id.routing_number.clone(),
            )),
            BankAccountInfo::Encrypted(_) => Err(AccountFromEntryError::DataIsEncrypted),
        }
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::{
        core::BankError,
        test_utils::{test_val::TestVal, MockTrustBankDispatcher, MockTxnIssuer},
    };
    use pseudo::Mock;
    use xand_api_client::BankAccountId;

    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;
    use xand_banks::models::BankBalance;

    pub fn build_pending_create_request(
        amount: u64,
        correlation_id: CorrelationId,
        account: &str,
    ) -> PendingCreateRequest {
        PendingCreateRequest {
            amount_in_minor_unit: amount,
            correlation_id,
            account: BankAccountId {
                routing_number: "test".to_string(),
                account_number: account.to_string(),
            }
            .into(),
            completing_transaction: None,
        }
    }

    #[test]
    fn completed_creates_dont_call_bank() {
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req_some =
            build_pending_create_request(500, [1; 16].into(), "member_acct");
        create_processor.receive_pending_creates(vec![pending_create_req_some.clone()]);
        assert!(dispatcher.mock_transfer_exists_for_create.called());
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::Completed
        );
        for _ in 0..10 {
            create_processor.receive_pending_creates(vec![pending_create_req_some.clone()]);
        }
        assert_eq!(dispatcher.mock_transfer_exists_for_create.num_calls(), 1);
    }

    #[test]
    fn same_create_request_doesnt_get_stored_twice() {
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "account");

        create_processor.receive_pending_creates(vec![pending_create_req.clone()]);
        let first_pending_creates_map = create_processor.in_progress_creates.clone();
        assert_eq!(create_processor.in_progress_creates.len(), 1);

        create_processor.receive_pending_creates(vec![pending_create_req]);

        assert_eq!(create_processor.in_progress_creates.len(), 1);
        assert_eq!(
            first_pending_creates_map,
            create_processor.in_progress_creates
        );
    }

    #[test]
    fn new_create_req_doesnt_replace_existing_entry() {
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "member_acct");

        create_processor.receive_pending_creates(vec![pending_create_req]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::Completed
        );

        // a create req with no matching transfer i.e. status == New
        let pending_create_req_2 = build_pending_create_request(600, [1; 16].into(), "member_acct");
        dispatcher.mock_transfer_exists_for_create.return_ok(false);
        create_processor.receive_pending_creates(vec![pending_create_req_2]);

        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::Completed
        );
    }

    #[test]
    fn create_req_with_no_transfer_does_nothing() {
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(false)),
            ..Default::default()
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "member_acct");
        create_processor.receive_pending_creates(vec![pending_create_req]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::New
        );
        assert!(!issuer.record_create.called());
    }

    #[test]
    fn create_req_with_matching_transfer_is_completed() {
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "member_acct");
        create_processor.receive_pending_creates(vec![pending_create_req.clone()]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::Completed
        );
        assert!(issuer.record_create.called());
        assert!(issuer.record_create.called_with(pending_create_req));
    }

    #[test]
    fn create_req_with_no_matching_transfer_is_retried() {
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(false)),
            ..Default::default()
        };
        let issuer = MockTxnIssuer::default();

        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "member_acct");
        create_processor.receive_pending_creates(vec![pending_create_req.clone()]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::New
        );
        assert!(!issuer.record_create.called());

        dispatcher.mock_transfer_exists_for_create.return_ok(true);
        create_processor.receive_pending_creates(vec![pending_create_req.clone()]);

        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::Completed
        );
        assert!(issuer.record_create.called());
        assert!(issuer.record_create.called_with(pending_create_req));
    }

    #[test]
    fn vanished_create_req_is_dropped() {
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(false)),
            ..Default::default()
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = build_pending_create_request(500, [1; 16].into(), "member_acct");
        create_processor.receive_pending_creates(vec![pending_create_req]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::New
        );
        assert!(!issuer.record_create.called());

        create_processor.receive_pending_creates(vec![]);
        assert!(create_processor.in_progress_creates.is_empty());
        assert!(!issuer.record_create.called());
    }

    #[test]
    fn create_request_with_bad_bank_data_is_cancelled() {
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);

        let pending_create_req = PendingCreateRequest {
            amount_in_minor_unit: 100,
            correlation_id: [1; 16].into(),
            account: BankAccountInfo::Encrypted(xand_api_client::EncryptionError::MessageMalformed),
            completing_transaction: None,
        };
        create_processor.receive_pending_creates(vec![pending_create_req.clone()]);
        assert_eq!(
            create_processor.in_progress_creates[&[1; 16].into()].status,
            CreateStatus::CancelRequested
        );
        assert!(issuer.cancel_create.called());
        assert!(issuer.cancel_create.called_with(pending_create_req));
    }

    #[test]
    fn bad_response_from_bank_does_not_result_in_cancel_or_fulfill() {
        // Given a create_processor, a create request, and a spurious error from the bank
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Err(BankError::UnknownError)),
            mock_transfer_exists_for_redeem: Mock::new(Ok(true)),
            mock_bank_exists: Mock::new(Ok(true)),
            mock_transfer: Mock::new(Ok(())),
            mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
            mock_is_account_allowlisted: Mock::new(true),
            mock_all_reserve_balances: Mock::new(HashMap::new()),
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id, "member_acct");

        // When you submit the create
        let pending_creates = vec![pending_create_req];
        create_processor.receive_pending_creates(pending_creates);

        // Then neither cancel nor fulfill is called
        assert!(!issuer.cancel_create.called());
        assert!(!issuer.record_create.called());
    }

    #[test]
    fn pending_create_is_valid_if_bank_exists() {
        let dispatcher = MockTrustBankDispatcher {
            mock_bank_exists: Mock::new(Ok(true)),
            ..Default::default()
        };
        let entry = StatusEntry {
            create_req: TestVal::test_val(),
            status: CreateStatus::New,
        };
        assert_eq!(invalid_reason(&dispatcher, &entry).unwrap(), None);
    }

    #[test]
    fn pending_create_is_not_valid_if_no_bank_exists() {
        let dispatcher = MockTrustBankDispatcher {
            mock_bank_exists: Mock::new(Ok(false)),
            ..Default::default()
        };
        let entry = StatusEntry {
            create_req: TestVal::test_val(),
            status: CreateStatus::New,
        };
        assert!(invalid_reason(&dispatcher, &entry).unwrap().is_some());
    }

    #[test]
    fn error_from_dispatcher_does_not_give_invalid_reason_but_error() {
        let dispatcher = MockTrustBankDispatcher {
            mock_bank_exists: Mock::new(Err(BankError::UnknownError)),
            ..Default::default()
        };
        let entry = StatusEntry {
            create_req: TestVal::test_val(),
            status: CreateStatus::New,
        };
        assert!(invalid_reason(&dispatcher, &entry).is_err());
    }

    #[test]
    fn invalid_create_requests_marked_cancelrequested_after_processing() {
        let dispatcher = MockTrustBankDispatcher {
            mock_bank_exists: Mock::new(Ok(false)),
            ..Default::default()
        };
        let issuer = MockTxnIssuer::default();
        let mut entry = StatusEntry {
            create_req: TestVal::test_val(),
            status: CreateStatus::New,
        };
        process_single_pending_create(&dispatcher, &issuer, &mut entry).unwrap();
        assert_eq!(entry.status, CreateStatus::CancelRequested);
    }

    #[test]
    fn logs_when_tracking_new_pending_create() {
        // Given a create_processor and a pending create
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you initially tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the "tracking new" event
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::TrackingNewPendingCreate { id }.debug_fmt(),
        )];
        assert!(events.contains(&expected_event));
    }

    #[test]
    fn does_not_log_duplicate_tracking_pending_create() {
        // Given a create_processor and a pending create
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates for the second time
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates.clone());
            create_processor.receive_pending_creates(pending_creates);
        });

        // There is only one tracking message
        let expected_event = vec![(
            "message".to_string(),
            LoggingEvent::TrackingNewPendingCreate { id }.debug_fmt(),
        )];
        assert!(events.into_iter().filter(|e| *e == expected_event).count() == 1);
    }

    #[test]
    fn receive_pending_creates__does_not_attempt_cancellation_of_cancelrequested_create() {
        // Given a create_processor, a pending create, a dispatcher with no bank
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(true)),
            mock_transfer_exists_for_redeem: Mock::new(Ok(true)),
            mock_bank_exists: Mock::new(Ok(false)),
            mock_transfer: Mock::new(Ok(())),
            mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
            mock_is_account_allowlisted: Mock::new(true),
            mock_all_reserve_balances: Mock::new(HashMap::new()),
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id, "member_acct");
        let pending_creates = vec![pending_create_req];

        // When the pending creates are processed and then the cancel-pending create is re-processed an arbitrary amount of times
        create_processor.receive_pending_creates(pending_creates);
        process_single_pending_create(
            &dispatcher,
            &issuer,
            &mut create_processor.in_progress_creates[&[1; 16].into()].clone(),
        )
        .unwrap();
        process_single_pending_create(
            &dispatcher,
            &issuer,
            &mut create_processor.in_progress_creates[&[1; 16].into()].clone(),
        )
        .unwrap();

        // Then the trust does not attempt multiple cancels to the chain
        assert_eq!(issuer.cancel_create.num_calls(), 1);
    }

    #[test]
    fn receive_pending_creates__logs_no_op_when_cancelrequested_create_cancellation_attempted() {
        // Given a create_processor, a pending create, a dispatcher with no bank
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(true)),
            mock_transfer_exists_for_redeem: Mock::new(Ok(true)),
            mock_bank_exists: Mock::new(Ok(false)),
            mock_transfer: Mock::new(Ok(())),
            mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
            mock_is_account_allowlisted: Mock::new(true),
            mock_all_reserve_balances: Mock::new(HashMap::new()),
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id, "member_acct");
        let pending_creates = vec![pending_create_req];

        // When the pending creates are processed and a cancelrequested create is attempted to be cancelled
        create_processor.receive_pending_creates(pending_creates); // marks the create without a bank "cancelrequested"
        let events = tracing_capture_event_fields!({
            // subsequent processing of an "cancelrequested" create
            process_single_pending_create(
                &dispatcher,
                &issuer,
                &mut create_processor.in_progress_creates[&[1; 16].into()].clone(),
            )
            .unwrap()
        });
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::AlreadyRequestedCreateCancellationNoOp { id: [1; 16].into() }.debug_fmt(),
        )];

        // Then the trust logs a no-op during cancellation attempts of the cancelrequested create
        assert!(events.contains(&expected_event));
    }

    #[test]
    fn receive_pending_creates__logs_cancellation() {
        // Given a create_processor, a pending create and a dispatcher with no bank
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(true)),
            mock_transfer_exists_for_redeem: Mock::new(Ok(true)),
            mock_bank_exists: Mock::new(Ok(false)),
            mock_transfer: Mock::new(Ok(())),
            mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
            mock_is_account_allowlisted: Mock::new(true),
            mock_all_reserve_balances: Mock::new(HashMap::new()),
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the `CancelCreate` event for the missing bank
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::CancellingCreate {
                reason: CreateCancellationReason::BankNotFound,
                id,
            }
            .debug_fmt(),
        )];
        assert!(events.contains(&expected_event));
    }

    #[test]
    fn receive_pending_creates__logs_processing_request() {
        // Given a create_processor and a create request
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the "processing pending create" event
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::ProcessingPendingCreate { id }.debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }

    #[test]
    fn receive_pending_creates__logs_found_matching_txn_for_create() {
        // Given a create_processor, a create request, and a corresponding bank transfer
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the "found matching bank transfer" event
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::FoundMatchingBankTransferForCreate { id }.debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }

    #[test]
    fn receive_pending_creates__logs_not_found_matching_txn_for_create() {
        // Given a create_processor, a create request, and no corresponding bank transfer
        let dispatcher = MockTrustBankDispatcher {
            mock_transfer_exists_for_create: Mock::new(Ok(false)),
            mock_transfer_exists_for_redeem: Mock::new(Ok(true)),
            mock_bank_exists: Mock::new(Ok(true)),
            mock_transfer: Mock::new(Ok(())),
            mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
            mock_is_account_allowlisted: Mock::new(true),
            mock_all_reserve_balances: Mock::new(HashMap::new()),
        };
        let issuer = MockTxnIssuer::default();
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the "did not find bank transfer" event
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::DidNotFindBankTransferForCreate { id }.debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }

    #[test]
    fn receive_pending_creates__logs_fulfillment_completed() {
        // Given a create_processor and a create request
        let (dispatcher, issuer) = (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
        let mut create_processor = InMemoryCreateProcessor::new(&dispatcher, &issuer);
        let id: CorrelationId = [1; 16].into();
        let pending_create_req = build_pending_create_request(500, id.clone(), "member_acct");

        // When you tell the create_processor to receive pending creates
        let pending_creates = vec![pending_create_req];
        let events = tracing_capture_event_fields!({
            create_processor.receive_pending_creates(pending_creates);
        });

        // Then it emits the "fulfillment completed" event
        let expected_event: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::FulfillmentCompletedForCreate { id }.debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }
}
