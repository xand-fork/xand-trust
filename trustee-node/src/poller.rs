use super::Result;
use crate::{
    core::{CreateProcessor, RedeemProcessor},
    errors::{TrustError, XandApiClient},
    logging_events::LoggingEvent,
    metrics::{
        count_outstanding, init_metrics, record_claims, OutstandingFiatReqs, TrustMetricsBag,
    },
    XandApiConfig,
};
use snafu::ResultExt;
use std::{sync::Arc, time::Duration};
use tokio::runtime::Runtime;
use xand_api_client::{
    models::Paging, PendingCreateRequest, PendingRedeemRequest, ReconnectingXandApiClient,
    XandApiClientTrait,
};

pub const DEFAULT_XAND_API_TIMEOUT_SECONDS: Duration = Duration::from_secs(30);

pub struct Poller<C: CreateProcessor, R: RedeemProcessor> {
    xand_client: Box<dyn XandApiClientTrait>,
    create_processor: C,
    redeem_processor: R,
    metrics: TrustMetricsBag,
    // TODO: Remove with https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/4413
    rt: Arc<Runtime>,
}

impl<C: CreateProcessor, R: RedeemProcessor> Poller<C, R> {
    pub fn new(
        rt: Arc<Runtime>,
        xand_api_config: XandApiConfig,
        create_processor: C,
        redeem_processor: R,
    ) -> Result<Self> {
        let xand_client = match xand_api_config.xand_api_jwt {
            Some(jwt) => {
                ReconnectingXandApiClient::stub_jwt(xand_api_config.xand_api_endpoint.clone(), jwt)
            }
            None => rt.block_on(ReconnectingXandApiClient::stub(
                xand_api_config.xand_api_endpoint,
            )),
        };

        let timeout_seconds = xand_api_config
            .timeout_seconds
            .unwrap_or(DEFAULT_XAND_API_TIMEOUT_SECONDS);
        let xand_client_with_timeout = xand_client.with_timeout(timeout_seconds);

        Ok(Poller {
            xand_client: Box::new(xand_client_with_timeout),
            create_processor,
            redeem_processor,
            metrics: init_metrics(),
            rt,
        })
    }

    #[cfg(test)]
    pub(crate) fn new_with_client(
        rt: Arc<Runtime>,
        xand_client: Box<dyn XandApiClientTrait>,
        create_processor: C,
        redeem_processor: R,
    ) -> Self {
        Poller {
            rt,
            xand_client,
            create_processor,
            redeem_processor,
            metrics: init_metrics(),
        }
    }

    pub(crate) fn metrics(&self) -> &TrustMetricsBag {
        &self.metrics
    }
}

impl<C: CreateProcessor, R: RedeemProcessor> Poller<C, R> {
    /// Call to verify that this poller can successfully query the chain. Also records total claims
    /// metrics when called.
    pub fn health_check(&self) -> Result<()> {
        let total_issuance = self
            .rt
            .block_on(self.xand_client.get_total_issuance())
            .context(XandApiClient {
                msg: "Error while fetching total claims during health check",
            })?;
        record_claims(&self.metrics, total_issuance.total_issued);
        Ok(())
    }

    fn get_outstanding_create_reqs(&self) -> Result<Vec<PendingCreateRequest>> {
        let resp = self.rt.block_on(
            self.xand_client
                .get_pending_create_requests(Some(Paging::all())),
        );
        match resp {
            Err(e) => {
                let err = TrustError::XandApiClient {
                    msg: "Error while polling for creates".to_string(),
                    source: e,
                };
                tracing::error!(message = ?LoggingEvent::ErrorPollingForCreates {
                    reason: err.clone()
                });
                Err(err)
            }
            Ok(res) => Ok(res.data),
        }
    }

    fn get_outstanding_redeem_reqs(&self) -> Result<Vec<PendingRedeemRequest>> {
        let resp = self.rt.block_on(
            self.xand_client
                .get_pending_redeem_requests(Some(Paging::all())),
        );
        match resp {
            Err(e) => {
                let err = TrustError::XandApiClient {
                    msg: "Error while polling for redeems".to_string(),
                    source: e,
                };
                tracing::error!(message = ?LoggingEvent::ErrorPollingForRedeems {
                    reason: err.clone()
                });
                Err(err)
            }
            Ok(res) => Ok(res.data),
        }
    }

    /// Poll for any outstanding create requests or unfulfilled redeems on startup
    #[tracing::instrument(skip_all)]
    pub fn poll_for_new_requests(&mut self) -> Result<()> {
        tracing::debug!(message = ?LoggingEvent::Polling);
        let create_req_state = self.get_outstanding_create_reqs()?;
        count_outstanding(
            &self.metrics,
            OutstandingFiatReqs::Creates(&create_req_state),
        );
        if !create_req_state.is_empty() {
            self.create_processor
                .receive_pending_creates(create_req_state);
        };

        let unfulfilled_redeem_state = self.get_outstanding_redeem_reqs()?;
        count_outstanding(
            &self.metrics,
            OutstandingFiatReqs::Redeems(&unfulfilled_redeem_state),
        );
        if !unfulfilled_redeem_state.is_empty() {
            self.redeem_processor
                .receive_unfulfilled_redeems(unfulfilled_redeem_state);
        };

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;
    use url::Url;
    use xand_api_client::{Blockstamp, TotalIssuance};

    use super::*;
    use crate::test_utils::{test_val::TestVal, FakeCreateProcessor, FakeRedeemProcessor};
    use xand_api_client::mock::MockXandApiClient;

    #[test]
    fn doesnt_crash_on_no_new_state() {
        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let mock_client = Box::<MockXandApiClient>::default();
        let mut trustee = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );

        assert!(trustee.poll_for_new_requests().is_ok())
    }

    #[test]
    fn doesnt_crash_with_disconnected_xand_api() {
        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let xand_api_config = XandApiConfig {
            xand_api_endpoint: Url::from_str("http://192.0.2.0:1234").unwrap(),
            xand_api_jwt: None,
            timeout_seconds: None,
        };

        let create_result = Poller::new(
            Arc::new(Runtime::new().unwrap()),
            xand_api_config,
            create_processor,
            redeem_processor,
        );

        create_result.unwrap();
    }

    #[test]
    fn state_changes_are_sent_to_create_processor() {
        let create_response = vec![
            PendingCreateRequest::new(100, [1; 16].into(), TestVal::test_val(), None),
            PendingCreateRequest::new(200, [2; 16].into(), TestVal::test_val(), None),
        ];
        let redeem_response =
            PendingRedeemRequest::new(300, [3; 16].into(), TestVal::test_val(), None);
        let mock_client = Box::<MockXandApiClient>::default();
        let mut create_processor = FakeCreateProcessor::default();
        let mut redeem_processor = FakeRedeemProcessor::default();
        create_processor.create_reqs = create_response;
        redeem_processor.unfulfilled_redeems = vec![redeem_response];
        let mut trustee = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        assert!(trustee.poll_for_new_requests().is_ok());
        assert_eq!(trustee.create_processor.create_reqs.len(), 2);
        assert_eq!(trustee.redeem_processor.unfulfilled_redeems.len(), 1);
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn health_check_records_total_claims() {
        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let mock_client = Box::<MockXandApiClient>::default();
        mock_client.total_issuance.return_value(Ok(TotalIssuance {
            total_issued: 4000,
            blockstamp: Blockstamp {
                block_number: 10,
                unix_timestamp_ms: 1667949150,
                is_stale: false,
            },
        }));
        let trustee = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        trustee.health_check().unwrap();
        assert_eq!(trustee.metrics.total_claims.get(), 4000.0);
    }
}
