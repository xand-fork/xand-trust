use crate::{
    bank_dispatcher::BankDispatcherErrors,
    errors::{HealthCheckErrorReport, TrustError},
    history_fetcher::HistoryFetchError,
};
use snafu::Snafu;
use std::collections::HashMap;
use trustee_node_config::Account;
use xand_api_client::{
    CorrelationId, CreateCancellationReason, PendingCreateRequest, PendingRedeemRequest,
    RedeemCancellationReason,
};
use xand_banks::{errors::XandBanksErrors, models::BankBalance, xand_money::MoneyError};

pub type Result<T, E = BankError> = std::result::Result<T, E>;

/// The bank dispatcher is responsible for routing calls to the correct bank
/// implementation based on the routing information associated with the account.
pub trait TrustBankDispatcher {
    /// Forwards to bank based on account.
    fn transfer_exists_for_create(
        &self,
        pending_create: PendingCreateRequest,
        account: Account,
    ) -> Result<bool, BankError>;

    /// Forwards to bank based on account.
    fn transfer_exists_for_redeem(
        &self,
        redeem: PendingRedeemRequest,
        account: Account,
    ) -> Result<bool, BankError>;

    /// Returns `true` if a bank can be found for given account
    fn bank_exists(&self, account: &Account) -> Result<bool, BankError>;

    /// Given an amount and an account to which to transfer funds, transfer the specified amount
    /// from the trust reserve account at the same bank as the provided account, to that account.
    ///
    /// Implementations *must* return a `BankError::InsufficientBalance` if there are not enough
    /// available funds to fulfill the redemption.
    ///
    /// The correlation id needs to be recorded in the transfer transaction because it is used by the trustee
    /// to match transfers with fulfillments.
    fn transfer(
        &self,
        amount: u64,
        to_account: Account,
        id: &CorrelationId,
    ) -> Result<(), BankError>;

    /// Fans out to get account info from all banks.
    ///
    /// Returns `routing_number -> balance`
    fn reserve_balances(&self) -> HashMap<String, Result<BankBalance, BankError>>;

    /// Fetches the reserve balance for a specific bank
    fn reserve_balance_for(&self, routing_num: &str) -> Result<BankBalance, BankError>;

    /// Returns true if the provided account has been allowlisted by a member
    fn is_allowlisted(&self, account: &Account) -> bool;
}

/// The TxnIssuer is responsible for constructing and signing transactions and then sending them
/// to the chain for validation.
pub trait TxnIssuer {
    /// Creates, signs and sends a create transaction to the chain.
    fn record_create(&self, pending_create: &PendingCreateRequest) -> Result<(), TrustError>;
    /// Creates, signs and sends a redeem fulfillment transaction to the chain.
    fn record_fulfilled_redeem(&self, redeem: &PendingRedeemRequest) -> Result<(), TrustError>;
    /// Cancels a pending create with given reason
    fn cancel_create(
        &self,
        create: &PendingCreateRequest,
        reason: CreateCancellationReason,
    ) -> Result<(), TrustError>;
    /// Cancels a pending redeem with the given reason
    fn cancel_redeem(
        &self,
        redeem: &PendingRedeemRequest,
        reason: RedeemCancellationReason,
    ) -> Result<(), TrustError>;
}

/// The redeem_processor executes the logic around fulfilling unfulfilled redeems. It will orchestrate the other
/// components to perform the appropriate checks and actions against the bank and chain.
pub trait RedeemProcessor {
    /// This function receives all the currently unfulfilled redeems according to the chain and performs
    /// the correct logic to fulfill them.
    fn receive_unfulfilled_redeems(&mut self, unfulfilled_redeems: Vec<PendingRedeemRequest>);
}

/// The create_processor executes the logic around resolving pending creates. It will orchestrate the other
/// components to perform the appropriate checks and actions against the bank and chain.
pub trait CreateProcessor {
    /// This function receives all the currently pending creates according to the chain and performs
    /// the correct logic to resolve them.
    fn receive_pending_creates(&mut self, create_reqs: Vec<PendingCreateRequest>);
}

/// Used to validate that the services are up and running for the trustee to be up and running.
pub trait ServicesChecker {
    /// Checks if we can access the services. This can either be due to configuration
    /// error or the service being down/unaccessible (e.g. ip allowlisting).
    fn check_services(&self) -> Result<(), HealthCheckErrorReport>;
}

#[derive(Debug, Clone, Snafu, Serialize)]
#[snafu(visibility(pub(crate)))]
pub enum BankError {
    #[snafu(display("Bank not found: {}", source))]
    BankNotFound { source: XandBanksErrors },
    #[snafu(display("Error fetching balance: {}", source))]
    BalanceError { source: XandBanksErrors },
    #[snafu(display("Error fetching history: {}", source))]
    HistoryError { source: XandBanksErrors },
    #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
    #[snafu(display("Error fetching history: {}", source))]
    FetchError { source: HistoryFetchError },
    #[snafu(display("Error performing transfer: {}", source))]
    TransferError { source: XandBanksErrors },
    #[snafu(display("Underlying error in bank client: {}", source))]
    UnderlyingXandBanksErrors { source: XandBanksErrors },
    #[snafu(display("Underlying error in bank dispatcher: {}", source))]
    #[snafu(context(false))]
    UnderlyingDispatcherError { source: BankDispatcherErrors },
    #[snafu(display("Failed to build bank dispatcher: {}", source))]
    BuildDispatcher { source: XandBanksErrors },
    #[snafu(display("Error in building date range for querying history: {}", source))]
    DurationError { source: XandBanksErrors },
    #[snafu(display("Insufficient balance to fulfill redeem"))]
    InsufficientBalance,

    // TODO: Should be serializable after
    //   https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5012
    #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
    #[snafu(
        display("Underlying error in money conversion: {}", source),
        context(false)
    )]
    UnderlyingMoneyError { source: MoneyError },

    #[cfg(test)]
    UnknownError,
}

/// Bug fix addressing MCB character limit length of 33.
/// Trim preceding "0x" if exists
/// ADO Item: https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5330
pub fn trim_0x_prefix(s: &str) -> String {
    s.trim_start_matches("0x").into()
}

#[cfg(test)]
mod trim_0x_tests {
    use super::*;

    #[test]
    fn trim_0x_prefix_removes_0x_prefix_if_exists() {
        // Given
        let id_str = "0x000102030405060708090a0b0c0d0e0f";

        // When
        let res = trim_0x_prefix(id_str);

        // Then
        assert_eq!(res, "000102030405060708090a0b0c0d0e0f");
    }

    #[test]
    fn trim_0x_prefix_returns_original_string_if_no_0x_prefix() {
        // Given
        let id_str = "000102030405060708090a0b0c0d0e0f";

        // When
        let res = trim_0x_prefix(id_str);

        // Then
        assert_eq!(res, "000102030405060708090a0b0c0d0e0f");
    }
}
