use thiserror::Error;
use tracing_subscriber::util::TryInitError;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    TracingInitError(#[from] TryInitError),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
}
