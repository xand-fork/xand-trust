use crate::{
    core::TxnIssuer,
    errors::{TrustError, XandApiClient},
    xand_api_config::XandApiConfig,
};
use snafu::ResultExt;
use std::sync::Arc;
use tokio::runtime::Runtime;
use xand_address::Address;
use xand_api_client::{
    CashConfirmation, CreateCancellation, CreateCancellationReason, PendingCreateRequest,
    PendingRedeemRequest, ReconnectingXandApiClient, RedeemCancellation, RedeemCancellationReason,
    RedeemFulfillment, TransactionStatus, XandApiClientTrait, XandTransaction,
};

pub struct XandIssuer<Client: XandApiClientTrait> {
    client: Client,
    // TODO: Remove with https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/4413
    rt: Arc<Runtime>,
    trust_address: Address,
}

impl XandIssuer<ReconnectingXandApiClient> {
    pub fn new(
        rt: Arc<Runtime>,
        xand_api_config: XandApiConfig,
        trust_address: Address,
    ) -> Result<Self, TrustError> {
        let client = match xand_api_config.xand_api_jwt {
            Some(jwt) => {
                ReconnectingXandApiClient::stub_jwt(xand_api_config.xand_api_endpoint, jwt)
            }
            None => rt.block_on(ReconnectingXandApiClient::stub(
                xand_api_config.xand_api_endpoint,
            )),
        };

        Ok(XandIssuer {
            client,
            rt,
            trust_address,
        })
    }
}

impl<C: XandApiClientTrait> TxnIssuer for XandIssuer<C> {
    fn record_create(&self, pending_create: &PendingCreateRequest) -> Result<(), TrustError> {
        let id = pending_create.correlation_id.clone();
        let txn = CashConfirmation { correlation_id: id };
        let error_context = "while fulfilling creation request";
        let resp = self
            .rt
            .block_on(self.client.submit_transaction_wait_final(
                self.trust_address.clone(),
                XandTransaction::CashConfirmation(txn),
            ))
            .context(XandApiClient { msg: error_context })?;
        transaction_status_is_finalized(resp.status, error_context)
    }

    fn record_fulfilled_redeem(&self, redeem: &PendingRedeemRequest) -> Result<(), TrustError> {
        let correlation_id = redeem.clone().correlation_id;
        let txn = RedeemFulfillment { correlation_id };
        let error_context = "while fulfilling redemption request";
        let resp = self
            .rt
            .block_on(self.client.submit_transaction_wait_final(
                self.trust_address.clone(),
                XandTransaction::RedeemFulfillment(txn),
            ))
            .context(XandApiClient { msg: error_context })?;
        transaction_status_is_finalized(resp.status, error_context)
    }

    fn cancel_create(
        &self,
        create: &PendingCreateRequest,
        reason: CreateCancellationReason,
    ) -> Result<(), TrustError> {
        let txn = CreateCancellation {
            correlation_id: create.correlation_id.clone(),
            reason,
        };
        let error_context = "while issuing create cancellation";
        let resp = self
            .rt
            .block_on(self.client.submit_transaction_wait(
                self.trust_address.clone(),
                XandTransaction::CreateCancellation(txn),
            ))
            .context(XandApiClient { msg: error_context })?;
        if let TransactionStatus::Invalid(reason) = resp.status {
            return Err(TrustError::TransactionInvalid {
                message: format!("{}: {:?}", error_context, reason),
            });
        };
        Ok(())
    }

    fn cancel_redeem(
        &self,
        redeem: &PendingRedeemRequest,
        reason: RedeemCancellationReason,
    ) -> Result<(), TrustError> {
        let txn = RedeemCancellation {
            correlation_id: redeem.correlation_id.clone(),
            reason,
        };
        let error_context = "while issuing redemption cancellation";
        let resp = self
            .rt
            .block_on(self.client.submit_transaction_wait_final(
                self.trust_address.clone(),
                XandTransaction::RedeemCancellation(txn),
            ))
            .context(XandApiClient { msg: error_context })?;
        transaction_status_is_finalized(resp.status, error_context)
    }
}

fn transaction_status_is_finalized(
    status: TransactionStatus,
    context: &str,
) -> Result<(), TrustError> {
    match status {
        TransactionStatus::Unknown => Err(TrustError::TransactionUnknown {
            message: context.into(),
        }),
        TransactionStatus::Pending => Err(TrustError::TransactionEndedPending {
            message: context.into(),
        }),
        TransactionStatus::Invalid(reason) => Err(TrustError::TransactionInvalid {
            message: format!("{}: {:?}", context, reason),
        }),
        TransactionStatus::Committed => Err(TrustError::TransactionEndedCommitted {
            message: context.into(),
        }),
        TransactionStatus::Finalized => Ok(()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::test_val::TestVal;
    use pseudo::Mock;
    use std::str::FromStr;
    use url::Url;
    use xand_api_client::{
        errors::XandApiClientError,
        mock::{finalized_status, MockXandApiClient},
    };

    #[test]
    fn recording_create_sends_to_chain() {
        let pending_create_req =
            PendingCreateRequest::new(500, [1; 16].into(), TestVal::test_val(), None);
        let issuer = XandIssuer {
            client: MockXandApiClient {
                submits: Mock::new(Ok(vec![Ok(finalized_status())])),
                ..Default::default()
            },
            trust_address: Address::from_str("5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7")
                .unwrap(),
            rt: Arc::new(Runtime::new().unwrap()),
        };
        let res = issuer.record_create(&pending_create_req);
        assert!(res.is_ok());
    }

    #[test]
    fn recording_fulfilled_redeem_sends_to_chain() {
        let redeem = PendingRedeemRequest::new(500, [2; 16].into(), TestVal::test_val(), None);
        let issuer = XandIssuer {
            client: MockXandApiClient {
                submits: Mock::new(Ok(vec![Ok(finalized_status())])),
                ..Default::default()
            },
            trust_address: Address::from_str("5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7")
                .unwrap(),
            rt: Arc::new(Runtime::new().unwrap()),
        };
        let res = issuer.record_fulfilled_redeem(&redeem);
        assert!(res.is_ok());
    }

    #[test]
    fn cancel_create_gets_ok() {
        let pending_create_req =
            PendingCreateRequest::new(500, [1; 16].into(), TestVal::test_val(), None);
        let issuer = XandIssuer {
            client: MockXandApiClient {
                submits: Mock::new(Ok(vec![Ok(finalized_status())])),
                ..Default::default()
            },
            trust_address: Address::from_str("5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7")
                .unwrap(),
            rt: Arc::new(Runtime::new().unwrap()),
        };
        let res = issuer.cancel_create(&pending_create_req, CreateCancellationReason::InvalidData);
        assert!(res.is_ok());
    }

    #[test]
    fn client_failures_bubble_up() {
        let redeem = PendingRedeemRequest::new(500, [3; 16].into(), TestVal::test_val(), None);
        let issuer = XandIssuer {
            client: MockXandApiClient {
                submits: Mock::new(Err(XandApiClientError::BadReplyError {
                    message: "Whoaaaa!".to_string(),
                })),
                ..Default::default()
            },
            trust_address: Address::from_str("5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7")
                .unwrap(),
            rt: Arc::new(Runtime::new().unwrap()),
        };
        assert!(issuer.record_fulfilled_redeem(&redeem).is_err());
    }

    #[test]
    fn doesnt_crash_with_disconnected_xand_api() {
        let xand_api_config = XandApiConfig {
            xand_api_endpoint: Url::from_str("http://192.0.2.0:1234").unwrap(),
            xand_api_jwt: None,
            timeout_seconds: None,
        };
        let issuer_create_result = XandIssuer::new(
            Arc::new(Runtime::new().unwrap()),
            xand_api_config,
            Address::from_str("5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7").unwrap(),
        );

        issuer_create_result.unwrap();
    }
}
