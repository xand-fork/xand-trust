use crate::{
    test_utils::{MockTrustBankDispatcher, MockTxnIssuer},
    InMemoryRedeemProcessor,
};
use std::time::Duration;
use xand_api_client::{BankAccountId, CorrelationId, PendingRedeemRequest};

pub fn build_redeem_processor<'a>(
    bank_dispatcher: &'a MockTrustBankDispatcher,
    txn_issuer: &'a MockTxnIssuer,
) -> InMemoryRedeemProcessor<'a, MockTrustBankDispatcher, MockTxnIssuer> {
    InMemoryRedeemProcessor::new(bank_dispatcher, txn_issuer, Duration::from_secs(0))
}

pub fn build_pending_redeem(
    amount: u64,
    correlation_id: CorrelationId,
    acct_name: &str,
) -> PendingRedeemRequest {
    PendingRedeemRequest::new(
        amount,
        correlation_id,
        BankAccountId {
            account_number: acct_name.to_string(),
            routing_number: "928173892".to_string(),
        }
        .into(),
        None,
    )
}
