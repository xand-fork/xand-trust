use super::super::*;
use crate::{
    core::BankError,
    errors::TrustError,
    redeem_processor::tests::{build_pending_redeem, build_redeem_processor},
    test_utils::{MockTrustBankDispatcher, MockTxnIssuer},
};
use fake_clock::FakeClock;
use pseudo::Mock;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;
use xand_api_client::BankAccountId;

const REDEEM_RETRY_DELAY_SECONDS: u64 = 1u64;

#[test]
fn when_it_receives_no_redeems_then_it_does_nothing() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());

    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 0);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);
}

#[test]
fn when_it_receives_a_new_redeem_and_no_transfer_exists_then_it_transfers_and_fulfills() {
    let expected_account = Account::new("the_hash".to_string(), "wilson".to_string());

    let redeem = PendingRedeemRequest::new(
        100,
        [1; 16].into(),
        BankAccountId {
            account_number: expected_account.account_number.clone(),
            routing_number: expected_account.routing_number.clone(),
        }
        .into(),
        None,
    );
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        ..Default::default()
    };
    let mock_issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    let transfers_call = &mock_dispatcher.mock_transfer.calls()[0];
    assert_eq!(transfers_call.0, 100);
    let account = &transfers_call.1;
    assert_eq!(account.account_number, expected_account.account_number);
    assert_eq!(account.routing_number, expected_account.routing_number);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 1);
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn when_it_receives_a_new_redeem_and_a_transfer_exists_then_it_fulfills() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());

    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 0);
    assert!(mock_issuer.record_fulfilled_redeem.called());
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn completed_redeems_dont_call_bank() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());

    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(
        mock_dispatcher.mock_transfer_exists_for_redeem.num_calls(),
        1
    );
    assert_eq!(
        redeem_processor.pending_redeems[&[1; 16].into()].status,
        RedeemStatus::Completed
    );
    for _ in 0..10 {
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    }
    assert_eq!(
        mock_dispatcher.mock_transfer_exists_for_redeem.num_calls(),
        1
    );
}

#[test]
fn when_it_receives_a_new_redeem_and_the_txn_check_fails_it_does_nothing() {
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Err(BankError::UnknownError)),
        ..Default::default()
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mock_issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 0);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);
}

#[test]
fn when_a_txn_check_fails_it_is_retried_next_call() {
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Err(BankError::UnknownError)),
        ..Default::default()
    };

    let expected_account = Account::new("the_hash".to_string(), "wilson".to_string());
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mock_issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 0);

    //Then we send it again and it works
    mock_dispatcher
        .mock_transfer_exists_for_redeem
        .return_ok(false);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    let transfers_call = &mock_dispatcher.mock_transfer.calls()[0];
    assert_eq!(transfers_call.0, 100);
    let account = &transfers_call.1;
    assert_eq!(account.account_number, expected_account.account_number);
    // TODO uncomment when we are no longer hardcoding Silvergate routing number - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5116
    // assert_eq!(account.routing_number, expected_account.routing_number);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 1);
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn if_a_transfer_returns_error_but_succeeds_quietly_funds_are_not_redeemed_twice() {
    // Given
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_transfer: Mock::new(Err(BankError::UnknownError)),
        ..Default::default()
    };
    let mock_issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let expected_account = Account::new("the_hash".to_string(), "wilson".to_string());
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_issuer.record_fulfilled_redeem.calls().len(), 0);

    // When
    mock_dispatcher
        .mock_transfer_exists_for_redeem
        .return_ok(true);
    mock_dispatcher.mock_transfer.return_ok(());
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);

    // Then
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    let transfers_call = &mock_dispatcher.mock_transfer.calls()[0];
    assert_eq!(transfers_call.0, 100);
    let account = &transfers_call.1;
    assert_eq!(account.account_number, expected_account.account_number);
    // TODO uncomment when we are no longer hardcoding Silvergate routing number - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5116
    // assert_eq!(account.routing_number, expected_account.routing_number);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 1);
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn when_a_redeem_fulfillment_fails_it_is_retried_next_call_but_transfer_is_not() {
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        ..Default::default()
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mock_issuer = MockTxnIssuer {
        record_fulfilled_redeem: Mock::new(Err(TrustError::FakeTestError)),
        ..Default::default()
    };

    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 1);

    //Then we send it again and it works
    mock_issuer.record_fulfilled_redeem.return_ok(());
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 2);
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[1];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn when_a_redeem_fulfillment_fails_it_is_retried_only_after_a_delay() {
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_transfer: Mock::new(Err(BankError::UnknownError)),
        ..Default::default()
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mock_issuer = MockTxnIssuer::default();

    let mut redeem_processor = InMemoryRedeemProcessor::new(
        &mock_dispatcher,
        &mock_issuer,
        Duration::from_secs(REDEEM_RETRY_DELAY_SECONDS),
    );
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);

    // advance into the future halfway towards the retry delay
    FakeClock::advance_time(REDEEM_RETRY_DELAY_SECONDS * 1000 / 2);

    // send the redeem again and verify no attempts to redeem are made
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);

    // advance past the redeem retry delay
    FakeClock::advance_time(REDEEM_RETRY_DELAY_SECONDS * 1000);

    //Then we send it again and it works
    mock_dispatcher.mock_transfer.return_ok(());
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 2);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 1);
    let txn_call = &mock_issuer.record_fulfilled_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
}

#[test]
fn redeem_fulfillment_retries_insufficient_balance() {
    // Given a transfer attempt which will fail for insufficient balance
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_transfer: Mock::new(Err(BankError::InsufficientBalance)),
        ..Default::default()
    };
    let id: CorrelationId = [1; 16].into();
    let redeem = build_pending_redeem(100, id, "the_hash");

    let mock_issuer = MockTxnIssuer::default();

    // When the redeem is initially attempted it is failed and sent to a retry state
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 1);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);

    // When the redeem retry delay passes
    FakeClock::advance_time(REDEEM_RETRY_DELAY_SECONDS * 1000);

    // Then the redeem is re-attempted
    redeem_processor.receive_unfulfilled_redeems(vec![redeem]);
    assert_eq!(mock_dispatcher.mock_transfer.num_calls(), 2);
    assert_eq!(mock_issuer.record_fulfilled_redeem.num_calls(), 0);
}

#[test]
fn when_a_redeem_has_bad_bank_data_then_it_is_cancelled() {
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_transfer: Mock::new(Err(BankError::InsufficientBalance)),
        ..Default::default()
    };
    let id: CorrelationId = [1; 16].into();
    let redeem = PendingRedeemRequest::new(
        100,
        id.clone(),
        BankAccountInfo::Encrypted(xand_api_client::EncryptionError::MessageMalformed),
        None,
    );

    let mock_issuer = MockTxnIssuer::default();

    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    assert_eq!(mock_issuer.cancel_redeem.num_calls(), 1);
    let txn_call = &mock_issuer.cancel_redeem.calls()[0];
    assert_eq!(txn_call.correlation_id, redeem.correlation_id);
    assert!(matches!(
        redeem_processor.pending_redeems[&id].status,
        RedeemStatus::Completed
    ));
}

#[test]
fn receive_unfulfilled_redeems__bank_acct_not_on_allowlist_redeem_cancelled_and_completed() {
    // Given a redeem request into a bank account that is not allowlisted
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_is_account_allowlisted: Mock::new(false),
        ..Default::default()
    };
    let id: CorrelationId = [1; 16].into();
    let redeem = build_pending_redeem(100, id.clone(), "the_hash");

    let mock_issuer = MockTxnIssuer::default();

    // When the pending redeems are processed
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);

    // Then no bank transfer happens
    assert_eq!(
        mock_dispatcher.mock_transfer_exists_for_redeem.num_calls(),
        0
    );

    // Then the redeem request is cancelled
    assert_eq!(mock_issuer.cancel_redeem.num_calls(), 1);
    let redeem_cancel_call = &mock_issuer.cancel_redeem.calls()[0];
    assert_eq!(redeem_cancel_call.correlation_id, redeem.correlation_id);
    // Then the transaction is considered complete
    assert!(matches!(
        redeem_processor.pending_redeems[&id].status,
        RedeemStatus::Completed
    ));
}

#[test]
fn receive_unfulfilled_redeems__bank_acct_not_on_allowlist_dont_call_bank_transfer() {
    // Given a redeem request into a bank account that is not allowlisted
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_is_account_allowlisted: Mock::new(false),
        ..Default::default()
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");

    let mock_issuer = MockTxnIssuer::default();

    // When the pending redeems are processed
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    redeem_processor.receive_unfulfilled_redeems(vec![redeem]);

    // Then the bank transfer is not called
    assert_eq!(
        mock_dispatcher.mock_transfer_exists_for_redeem.num_calls(),
        0
    );
}

#[test]
fn receive_unfulfilled_redeems__bank_acct_not_on_allowlist_logs_cancellation() {
    // Given a redeem request into a bank account that is not allowlisted
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_is_account_allowlisted: Mock::new(false),
        ..Default::default()
    };
    let id: CorrelationId = [1; 16].into();
    let redeem = build_pending_redeem(100, id.clone(), "the_hash");
    // Given that we therefore expect the redeem to be cancelled
    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::CancellationIssuedForRedeem {
            id,
            reason: "AccountNotAllowed".to_string(),
        }
        .debug_fmt(),
    )];

    // When the pending redeems are processed
    let mock_issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);
    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    // Then the redeem request is cancelled
    assert!(events.contains(&expected_event));
}

#[test]
fn vanished_redemption_request_is_dropped() {
    let dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        ..Default::default()
    };
    let issuer = MockTxnIssuer::default();
    let mut redeem_processor = build_redeem_processor(&dispatcher, &issuer);
    let id = CorrelationId::from([1; 16]);
    redeem_processor.receive_unfulfilled_redeems(vec![build_pending_redeem(
        500,
        id.clone(),
        "ap_acct",
    )]);
    assert!(redeem_processor.pending_redeems.contains_key(&id));
    redeem_processor.receive_unfulfilled_redeems(Vec::new());
    assert!(!redeem_processor.pending_redeems.contains_key(&id));
}
