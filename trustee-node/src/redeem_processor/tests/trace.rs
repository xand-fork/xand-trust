use crate::{
    core::{BankError, RedeemProcessor},
    redeem_processor::tests::{build_pending_redeem, build_redeem_processor},
    test_utils::{MockTrustBankDispatcher, MockTxnIssuer},
    LoggingEvent, TrustError,
};
use pseudo::Mock;
use std::collections::HashMap;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;
use xand_api_client::{CorrelationId, RedeemCancellationReason};
use xand_banks::models::BankBalance;

/// Redeem Process Begin Test
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID
#[test]
fn receive_unfulfilled_redeems__logs_redeem_process_begin() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::ProcessingPendingRedeem {
            id: redeem.correlation_id,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

/// Redeem Process Begin Test (twice)
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID
#[test]
fn receive_unfulfilled_redeems__does_not_log_fulfillment_for_unfulfilled_requests() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let unexpected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::IssuingFulfillmentForRedeem {
            id: CorrelationId::from([1; 16]),
        }
        .debug_fmt(),
    )];
    assert!(!events.contains(&unexpected_event));
}

///     BOOK TRANSFER
/// - Book Transfer Success
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, book-transfer ID
#[test]
fn receive_unfulfilled_redeems__logs_book_transfer_success() {
    let mock_issuer = MockTxnIssuer::default();
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_create: Mock::new(Ok(true)),
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_bank_exists: Mock::new(Ok(true)),
        mock_transfer: Mock::new(Ok(())),
        mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
        mock_is_account_allowlisted: Mock::new(true),
        mock_all_reserve_balances: Mock::new(HashMap::new()),
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::BankTransferCompletedForRedeem {
            id: redeem.correlation_id,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}
/// Book Transfer Failure
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, book-transfer ID
#[test]
fn receive_unfulfilled_redeems__logs_book_transfer_failure() {
    let mock_issuer = MockTxnIssuer::default();
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_create: Mock::new(Ok(true)),
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_bank_exists: Mock::new(Ok(true)),
        mock_transfer: Mock::new(Err(BankError::UnknownError)),
        mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
        mock_is_account_allowlisted: Mock::new(true),
        mock_all_reserve_balances: Mock::new(HashMap::new()),
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::BankTransferFailedForRedeem {
            id: redeem.correlation_id,
            reason: BankError::UnknownError,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

///     XAND API TRANSACTION (FULFILLMENT)
/// - Redeem Fulfillment Against XandAPI Success
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, redeem fulfillment tx ID
#[test]
fn receive_unfulfilled_redeems__logs_fulfillment_tx_success() {
    let (mock_dispatcher, mock_issuer) =
        (MockTrustBankDispatcher::default(), MockTxnIssuer::default());
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::FulfillmentIssuedForRedeem {
            id: redeem.correlation_id,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

/// Redeem Fulfillment Against XandAPI Failure
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, redeem fulfillment tx ID
#[test]
fn receive_unfulfilled_redeems__logs_fulfillment_tx_failure() {
    let mock_issuer = MockTxnIssuer {
        record_fulfilled_redeem: Mock::new(Err(TrustError::FakeTestError)),
        record_create: Mock::new(Ok(())),
        cancel_create: Mock::new(Ok(())),
        cancel_redeem: Mock::new(Ok(())),
    };
    let mock_dispatcher = MockTrustBankDispatcher::default();
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::ErrorIssuingFulfillmentForRedeem {
            redeem,
            err: TrustError::FakeTestError,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

///     XAND API TRANSACTION (CANCELLATION)
/// - Redeem Cancellation Against XandAPI Success
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, redeem cancellation tx ID
#[test]
fn receive_unfulfilled_redeems__logs_cancellation_tx_success() {
    let mock_issuer = MockTxnIssuer::default();
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_create: Mock::new(Ok(true)),
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_bank_exists: Mock::new(Ok(true)),
        mock_transfer: Mock::new(Ok(())),
        mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
        mock_is_account_allowlisted: Mock::new(false),
        mock_all_reserve_balances: Mock::new(HashMap::new()),
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::CancellationIssuedForRedeem {
            id: redeem.correlation_id,
            reason: RedeemCancellationReason::AccountNotAllowed.to_string(),
        }
        .debug_fmt(),
    )];

    assert!(events.contains(&expected_event));
}

///  Redeem Cancellation Against XandAPI Failure
///      Properties needed in span:
///          - datetime stamp, correlation ID, transaction ID, redeem cancellation tx ID
#[test]
fn receive_unfulfilled_redeems__logs_cancellation_tx_failure() {
    let mock_issuer = MockTxnIssuer {
        record_fulfilled_redeem: Mock::new(Ok(())),
        record_create: Mock::new(Ok(())),
        cancel_create: Mock::new(Ok(())),
        cancel_redeem: Mock::new(Err(TrustError::FakeTestError)),
    };
    let mock_dispatcher = MockTrustBankDispatcher {
        mock_transfer_exists_for_create: Mock::new(Ok(true)),
        mock_transfer_exists_for_redeem: Mock::new(Ok(false)),
        mock_bank_exists: Mock::new(Ok(true)),
        mock_transfer: Mock::new(Ok(())),
        mock_reserve_balance_for: Mock::new(Ok(BankBalance::default())),
        mock_is_account_allowlisted: Mock::new(false),
        mock_all_reserve_balances: Mock::new(HashMap::new()),
    };
    let redeem = build_pending_redeem(100, [1; 16].into(), "the_hash");
    let mut redeem_processor = build_redeem_processor(&mock_dispatcher, &mock_issuer);

    let events = tracing_capture_event_fields!({
        redeem_processor.receive_unfulfilled_redeems(vec![redeem.clone()]);
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvent::ErrorDuringCancellationForRedeem {
            redeem,
            err: TrustError::FakeTestError,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}
