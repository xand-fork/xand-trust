pub(crate) mod async_error;

use crate::{
    bank_dispatcher::BankDispatcherErrors, config_reader::ConfigLoaderError,
    errors::async_error::AsyncError,
};
use snafu::Snafu;
use std::fmt::Display;
use xand_api_client::errors::XandApiClientError;

#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub))]
pub enum TrustError {
    #[snafu(display("{}", source))]
    Async { source: AsyncError },
    #[snafu(display("Configuration error: {}", source))]
    ConfigError { source: ConfigLoaderError },
    #[snafu(display("Underlying client error ({}): {}", msg, source))]
    XandApiClient {
        msg: String,
        source: XandApiClientError,
    },
    #[snafu(display("Underlying error in bank dispatcher: {}", source))]
    #[snafu(context(false))]
    UnderlyingDispatcherError { source: BankDispatcherErrors },
    #[snafu(display("Error with bank: {}", source))]
    BankError { source: crate::core::BankError },
    #[snafu(display("Error submitting transaction: {}", message))]
    TransactionInvalid { message: String },
    #[snafu(display("Transaction is in an unknown state ({})", message))]
    TransactionUnknown { message: String },
    #[snafu(display("Transaction ended in pending state ({})", message))]
    TransactionEndedPending { message: String },
    #[snafu(display("Transaction ended in committed state ({})", message))]
    TransactionEndedCommitted { message: String },
    #[snafu(display("Unexpected state reached: ({})", message))]
    LogicError { message: String },
    #[snafu(display("Health check failed: {}", report))]
    HealthCheckError { report: HealthCheckErrorReport },
    #[snafu(display("Error retrieving from secret store: {:?}", message))]
    ReadSecretError { message: String },
    #[snafu(display("Error with tokio runtime: {}", message))]
    RuntimeError { message: String },
    #[cfg(test)]
    FakeTestError,
    #[snafu(display("Bank Transaction Repository Initialization Error: {}", message))]
    BtrInitializationError { message: String },
    #[snafu(display("Database initialization error for '{}': {}", path, message))]
    DbInitializationError { message: String, path: String },
}

#[derive(Debug, Clone, Serialize)]
pub struct HealthCheckErrorReport {
    service_errors: Vec<TrustError>,
}

impl From<Vec<TrustError>> for HealthCheckErrorReport {
    fn from(service_errors: Vec<TrustError>) -> Self {
        Self { service_errors }
    }
}

impl Display for HealthCheckErrorReport {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{:?}",
            self.service_errors
                .iter()
                .map(|e| format!("{}", e))
                .collect::<Vec<_>>()
        )
    }
}

impl From<ConfigLoaderError> for TrustError {
    fn from(e: ConfigLoaderError) -> Self {
        TrustError::ConfigError { source: e }
    }
}

impl From<crate::core::BankError> for TrustError {
    fn from(e: crate::core::BankError) -> Self {
        TrustError::BankError { source: e }
    }
}

impl From<xand_secrets::ReadSecretError> for TrustError {
    fn from(e: xand_secrets::ReadSecretError) -> Self {
        Self::ReadSecretError {
            message: format!("{:?}", e),
        }
    }
}

impl From<bank_transaction_repository::error::Error> for TrustError {
    fn from(e: bank_transaction_repository::error::Error) -> Self {
        TrustError::BtrInitializationError {
            message: e.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use crate::errors::TrustError;

    use super::HealthCheckErrorReport;

    use insta::assert_display_snapshot;

    #[test]
    fn health_check_error_report__display_returns_bracketed_list() {
        let error = HealthCheckErrorReport::from(vec![
            TrustError::LogicError {
                message: "this is a message".into(),
            },
            TrustError::FakeTestError,
        ]);

        assert_display_snapshot!(error, @r###"["Unexpected state reached: (this is a message)", "FakeTestError"]"###);
    }
}
