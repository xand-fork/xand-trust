use crate::{
    core::{BankError, CreateProcessor, RedeemProcessor, TrustBankDispatcher, TxnIssuer},
    errors::TrustError,
};
use pseudo::Mock;
use std::collections::HashMap;
use trustee_node_config::Account;
use xand_address::Address;
use xand_api_client::{
    errors::XandApiClientError,
    models::{Paginated, Paging},
    Blockstamp, CorrelationId, CreateCancellationReason, HealthResponse, PendingCreateRequest,
    PendingRedeemRequest, RedeemCancellationReason, TotalIssuance, ValidatorEmissionProgress,
    ValidatorEmissionRate, XandApiClientTrait,
};
use xand_banks::models::BankBalance;

pub(crate) mod test_val;
#[derive(Clone)]
pub struct MockTrustBankDispatcher {
    pub mock_transfer_exists_for_create:
        Mock<(PendingCreateRequest, Account), Result<bool, BankError>>,
    pub mock_transfer_exists_for_redeem:
        Mock<(PendingRedeemRequest, Account), Result<bool, BankError>>,
    pub mock_bank_exists: Mock<(), Result<bool, BankError>>,
    pub mock_transfer: Mock<(u64, Account), Result<(), BankError>>,
    pub mock_reserve_balance_for: Mock<String, Result<BankBalance, BankError>>,
    pub mock_is_account_allowlisted: Mock<Account, bool>,
    pub mock_all_reserve_balances: Mock<(), HashMap<String, Result<BankBalance, BankError>>>,
}

impl MockTrustBankDispatcher {
    pub fn new(
        create_res: Result<bool, BankError>,
        redeem_res: Result<bool, BankError>,
        mock_bank_exists: Result<bool, BankError>,
        transfer_res: Result<(), BankError>,
        balance_res: Result<BankBalance, BankError>,
        account_allowlisted: bool,
        all_reserve_balances: HashMap<String, Result<BankBalance, BankError>>,
    ) -> Self {
        Self {
            mock_transfer_exists_for_create: Mock::new(create_res),
            mock_transfer_exists_for_redeem: Mock::new(redeem_res),
            mock_bank_exists: Mock::new(mock_bank_exists),
            mock_transfer: Mock::new(transfer_res),
            mock_reserve_balance_for: Mock::new(balance_res),
            mock_is_account_allowlisted: Mock::new(account_allowlisted),
            mock_all_reserve_balances: Mock::new(all_reserve_balances),
        }
    }
}

impl Default for MockTrustBankDispatcher {
    fn default() -> Self {
        Self::new(
            Ok(true),
            Ok(true),
            Ok(true),
            Ok(()),
            Ok(BankBalance::default()),
            true,
            HashMap::new(),
        )
    }
}

impl TrustBankDispatcher for MockTrustBankDispatcher {
    fn transfer_exists_for_create(
        &self,
        pending_create: PendingCreateRequest,
        account: Account,
    ) -> Result<bool, BankError> {
        self.mock_transfer_exists_for_create
            .call((pending_create, account))
    }

    fn transfer_exists_for_redeem(
        &self,
        redeem: PendingRedeemRequest,
        account: Account,
    ) -> Result<bool, BankError> {
        self.mock_transfer_exists_for_redeem.call((redeem, account))
    }

    fn bank_exists(&self, _account: &Account) -> Result<bool, BankError> {
        self.mock_bank_exists.call(())
    }

    fn transfer(
        &self,
        amount: u64,
        to_account: Account,
        _id: &CorrelationId,
    ) -> Result<(), BankError> {
        self.mock_transfer.call((amount, to_account))
    }

    fn reserve_balances(&self) -> HashMap<String, Result<BankBalance, BankError>> {
        self.mock_all_reserve_balances.call(())
    }

    fn reserve_balance_for(&self, routing_num: &str) -> Result<BankBalance, BankError> {
        self.mock_reserve_balance_for.call(routing_num.to_string())
    }

    fn is_allowlisted(&self, account: &Account) -> bool {
        self.mock_is_account_allowlisted.call(account.clone())
    }
}

pub struct MockTxnIssuer {
    pub record_fulfilled_redeem: Mock<PendingRedeemRequest, Result<(), TrustError>>,
    pub record_create: Mock<PendingCreateRequest, Result<(), TrustError>>,
    pub cancel_create: Mock<PendingCreateRequest, Result<(), TrustError>>,
    pub cancel_redeem: Mock<PendingRedeemRequest, Result<(), TrustError>>,
}

impl Default for MockTxnIssuer {
    fn default() -> Self {
        Self {
            record_fulfilled_redeem: Mock::new(Ok(())),
            record_create: Mock::new(Ok(())),
            cancel_create: Mock::new(Ok(())),
            cancel_redeem: Mock::new(Ok(())),
        }
    }
}

impl TxnIssuer for MockTxnIssuer {
    fn record_create(&self, pending_create: &PendingCreateRequest) -> Result<(), TrustError> {
        self.record_create.call(pending_create.clone())
    }
    fn record_fulfilled_redeem(&self, redeem: &PendingRedeemRequest) -> Result<(), TrustError> {
        self.record_fulfilled_redeem.call(redeem.clone())
    }

    fn cancel_create(
        &self,
        create: &PendingCreateRequest,
        _: CreateCancellationReason,
    ) -> Result<(), TrustError> {
        self.cancel_create.call(create.clone())
    }

    fn cancel_redeem(
        &self,
        redeem: &PendingRedeemRequest,
        _: RedeemCancellationReason,
    ) -> Result<(), TrustError> {
        self.cancel_redeem.call(redeem.clone())
    }
}

#[derive(Default)]
pub struct FakeCreateProcessor {
    pub create_reqs: Vec<PendingCreateRequest>,
}

impl CreateProcessor for FakeCreateProcessor {
    fn receive_pending_creates(&mut self, create_reqs: Vec<PendingCreateRequest>) {
        self.create_reqs.extend(create_reqs);
    }
}

#[derive(Default)]
pub struct FakeRedeemProcessor {
    pub unfulfilled_redeems: Vec<PendingRedeemRequest>,
}

impl RedeemProcessor for FakeRedeemProcessor {
    fn receive_unfulfilled_redeems(&mut self, unfulfilled_redeems: Vec<PendingRedeemRequest>) {
        self.unfulfilled_redeems.extend(unfulfilled_redeems);
    }
}

#[allow(clippy::type_complexity)]
pub struct ConfigurableMockXandApiClient {
    pub get_pending_creation_requests:
        Mock<(Option<Paging>,), Result<Paginated<Vec<PendingCreateRequest>>, XandApiClientError>>,
    pub get_total_issuance: Mock<(), Result<TotalIssuance, XandApiClientError>>,
}

impl Default for ConfigurableMockXandApiClient {
    fn default() -> Self {
        Self {
            get_pending_creation_requests: Mock::new(Ok(vec![].into())),
            get_total_issuance: Mock::new(Ok(TotalIssuance {
                total_issued: 0,
                blockstamp: Blockstamp {
                    block_number: 0,
                    unix_timestamp_ms: 0,
                    is_stale: false,
                },
            })),
        }
    }
}

#[async_trait::async_trait]
impl XandApiClientTrait for ConfigurableMockXandApiClient {
    async fn submit_transaction(
        &self,
        _issuer: xand_address::Address,
        _txn: xand_api_client::XandTransaction,
    ) -> Result<xand_api_client::TransactionStatusStream, XandApiClientError> {
        unimplemented!();
    }

    async fn get_transaction_details(
        &self,
        _id: &xand_api_client::TransactionId,
    ) -> Result<xand_api_client::Transaction, XandApiClientError> {
        unimplemented!();
    }

    async fn get_transaction_history(
        &self,
        _paging: Option<Paging>,
        _filter: &xand_api_client::TransactionFilter,
    ) -> Result<Paginated<Vec<xand_api_client::Transaction>>, XandApiClientError> {
        unimplemented!();
    }

    async fn get_balance(&self, _address: &str) -> Result<Option<u128>, XandApiClientError> {
        Ok(Some(0))
    }

    async fn get_total_issuance(&self) -> Result<TotalIssuance, XandApiClientError> {
        self.get_total_issuance.call(())
    }

    async fn get_address_transactions(
        &self,
        _address: &str,
        _paging: Option<Paging>,
    ) -> Result<xand_api_client::TransactionHistoryPage, XandApiClientError> {
        unimplemented!();
    }

    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>, XandApiClientError> {
        self.get_pending_creation_requests.call((paging,))
    }

    async fn get_pending_redeem_requests(
        &self,
        _paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>, XandApiClientError> {
        unimplemented!();
    }

    async fn propose_action(
        &self,
        _issuer: xand_address::Address,
        _admin_txn: xand_api_client::AdministrativeTransaction,
    ) -> Result<xand_api_client::TransactionStatusStream, XandApiClientError> {
        unimplemented!();
    }

    async fn vote_on_proposal(
        &self,
        _issuer: xand_address::Address,
        _voting_txn: xand_api_client::VoteProposal,
    ) -> Result<xand_api_client::TransactionStatusStream, XandApiClientError> {
        unimplemented!();
    }

    async fn get_current_block(&self) -> Result<xand_api_client::Blockstamp, XandApiClientError> {
        unimplemented!();
    }

    async fn get_proposal(
        &self,
        _id: u32,
    ) -> Result<xand_api_client::Proposal, XandApiClientError> {
        unimplemented!();
    }

    async fn get_all_proposals(
        &self,
    ) -> Result<Vec<xand_api_client::Proposal>, XandApiClientError> {
        unimplemented!();
    }

    async fn get_members(&self) -> Result<Vec<xand_address::Address>, XandApiClientError> {
        unimplemented!();
    }

    async fn get_authority_keys(&self) -> Result<Vec<xand_address::Address>, XandApiClientError> {
        unimplemented!();
    }

    async fn get_trustee(&self) -> Result<xand_address::Address, XandApiClientError> {
        unimplemented!();
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_allowlist(
        &self,
    ) -> Result<Vec<(xand_address::Address, xand_api_client::CidrBlock)>, XandApiClientError> {
        unimplemented!();
    }

    async fn get_validator_emission_rate(
        &self,
    ) -> Result<ValidatorEmissionRate, XandApiClientError> {
        unimplemented!()
    }

    async fn get_validator_emission_progress(
        &self,
        _address: Address,
    ) -> Result<ValidatorEmissionProgress, XandApiClientError> {
        unimplemented!()
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u64, XandApiClientError> {
        unimplemented!()
    }

    async fn check_health(&self) -> Result<HealthResponse, XandApiClientError> {
        unimplemented!()
    }
}
