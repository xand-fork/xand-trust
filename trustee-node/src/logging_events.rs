use trustee_node_config::Account;
use xand_api_client::{CorrelationId, CreateCancellationReason, PendingRedeemRequest};

use crate::errors::HealthCheckErrorReport;

#[derive(Debug)]
pub enum LoggingEvent {
    QueryingBankTransactionRepository,
    BankTransactionRepositorySuccessfullyQueried,
    ReserveBalanceInsufficient {
        routing_number: String,
    },
    TrustStarted,
    /// No banks were present in the configuration file when we validated it
    NoBanksInConfig,
    /// A member issued a redemption request to an account that isn't allowlisted in the trust
    /// configuration file.
    AccountIsNotAllowlisted {
        account: Account,
    },
    TrustError {
        err: crate::errors::TrustError,
    },
    HealthCheckSuccessful,
    HealthCheckFailed {
        report: HealthCheckErrorReport,
    },
    BankHealthCheckPassed {
        routing_number: String,
    },
    BankHealthCheckFailed {
        routing_number: String,
    },
    XandApiHealthCheckPassed,
    XandApiHealthCheckFailed,
    ErrorPollingForCreates {
        reason: crate::errors::TrustError,
    },
    ErrorPollingForRedeems {
        reason: crate::errors::TrustError,
    },
    Polling,
    ReceivedUnfulfilledRedeems {
        num_redeems: usize,
    },
    ProcessingPendingRedeem {
        id: CorrelationId,
    },
    RemovedDuplicatePendingRedeems {
        removed_id_list: Vec<CorrelationId>,
    },
    AddedNewPendingRedeem {
        id: CorrelationId,
    },
    InitiatingBankTransferForRedeem {
        id: CorrelationId,
    },
    BankTransferCompletedForRedeem {
        id: CorrelationId,
    },
    BankTransferFailedForRedeem {
        id: CorrelationId,
        reason: crate::core::BankError,
    },
    BankTransferFoundForRedeem {
        id: CorrelationId,
    },
    IssuingFulfillmentForRedeem {
        id: CorrelationId,
    },
    FulfillmentIssuedForRedeem {
        id: CorrelationId,
    },
    IssuingCancellationForRedeem {
        id: CorrelationId,
        reason: String,
    },
    CancellationIssuedForRedeem {
        id: CorrelationId,
        reason: String,
    },
    ErrorIssuingFulfillmentForRedeem {
        redeem: PendingRedeemRequest,
        err: crate::errors::TrustError,
    },
    ErrorDuringCancellationForRedeem {
        redeem: PendingRedeemRequest,
        err: crate::errors::TrustError,
    },
    PendingRedeemRequiresCancellation {
        redeem: PendingRedeemRequest,
        message: String,
    },
    ErrorCheckingTxnForRedeem {
        redeem: PendingRedeemRequest,
        reason: crate::core::BankError,
    },
    ReceivedPendingCreates {
        num_creates: usize,
    },
    TrackingNewPendingCreate {
        id: CorrelationId,
    },
    RemoveTrackedPendingCreate {
        id: CorrelationId,
    },
    ProcessingPendingCreate {
        id: CorrelationId,
    },
    LookingForMatchingTxnForCreate {
        id: CorrelationId,
        amount: u64,
        bank_routing_number: String,
    },
    FoundMatchingBankTransferForCreate {
        id: CorrelationId,
    },
    DidNotFindBankTransferForCreate {
        id: CorrelationId,
    },
    IssuingFulfillmentForCreate {
        id: CorrelationId,
    },
    FulfillmentCompletedForCreate {
        id: CorrelationId,
    },
    MetricsError {
        msg: String,
    },
    AlreadyRequestedCreateCancellationNoOp {
        id: CorrelationId,
    },
    CancellingCreate {
        reason: CreateCancellationReason,
        id: CorrelationId,
    },
    CreateCancelled {
        id: CorrelationId,
    },
}
