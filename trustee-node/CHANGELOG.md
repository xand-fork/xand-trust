# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [9.0.0] 2022-04-01
### Changed
* The bank configuration so that all configurations with valid bank adapters are accepted and properly set up with BTR.

## [7.3.0] 2022-03-01
### Added
* Support for validator redemption

## [7.2.0] 2022-02-09
### Changed
* Xand Trust config's `partner_accounts` now contains a HashMap of `validators_to_accounts`

### Unchanged
* xand-trust-node functionality

## [Unreleased]
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->



<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->
[Unreleased]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.25.0...master
[0.25.0]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.24.2...0.25.0
[0.23.2]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.22.0...0.23.2
