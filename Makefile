.PHONY: start stop purge test run

GENERATED_NETWORK_DIR := ./xng/generated

# GENERATE NETWORK =====================

## Variables set for xng network generation
CHAIN_SPECVERSION := "11.0.2"
ZIP_FILENAME := "chain-spec-template.${CHAIN_SPECVERSION}.zip"
CHAINSPEC_TEMPLATES_DIR=./xng/chainspec_templates/
XNG_CONFIG_FILEPATH=./xng/xng.yaml
##

## Variables set for generating the trustee-node binary within a docker container for integration testing
DOCKER_TRUST_FOLDER=.docker/integ-test/

install-xng:
# TODO enable xng version passing as param
	cargo install --registry tpfs --version 3.0.0 xand_network_generator

download-chain-spec:
	wget --directory-prefix=${CHAINSPEC_TEMPLATES_DIR} --user=${ARTIFACTORY_USER}  --password=${ARTIFACTORY_PASS} \
		https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${ZIP_FILENAME}

generate:
	xand_network_generator generate  \
        --config ${XNG_CONFIG_FILEPATH} \
        --chainspec-zip ${CHAINSPEC_TEMPLATES_DIR}/${ZIP_FILENAME} \
        --output-dir ${GENERATED_NETWORK_DIR}
	touch ${GENERATED_NETWORK_DIR}/.env # Touch a .env file because `network_maestro` in integ-tests require one

# TRUSTEE image
build-trustee-docker-image: build-trustee-release-binary
	cd $(DOCKER_TRUST_FOLDER); make build

build-trustee-release-binary:
	cd $(DOCKER_TRUST_FOLDER); make run-compiler

# RUN TESTS =====================
start:
	cd $(GENERATED_NETWORK_DIR); docker-compose up -d --scale trustee=0

stop:
	cd $(GENERATED_NETWORK_DIR); docker-compose down

purge: stop
	cd $(GENERATED_NETWORK_DIR); find ./ -type d -name "chains" -exec sudo rm -rf {} +

test:
	cargo test

TEST_ARGS=""
integ-test: build-trustee-docker-image start
	sleep 10 # Wait for nodes to start up. Ideally this should instead poll until the network is ready.
	cargo test --all-features --test trust_integ -- --test-threads=1 $(TEST_ARGS)

run: purge integ-test

run-verbose:
	$(MAKE) run TEST_ARGS="--nocapture"

# PUBLISHING =====================

tag-release:
	./.ci/tag_release.sh

publish-patch:
	./.ci/prepare_release.sh patch

publish-minor:
	./.ci/prepare_release.sh minor

publish-major:
	./.ci/prepare_release.sh major

# LINTING + Testing ========================

clippy:
	cargo clippy --all-targets --all-features -- -D warnings

nest:
	cargo +nightly fmt -- --config imports_granularity="Crate"
