use std::collections::{HashMap, HashSet};

use bank_transaction_repository::SynchronizationPolicyConfiguration;
use insta::assert_yaml_snapshot;
use std::iter::FromIterator;
use std::time::Duration;
use trustee_node_config::{
    trust_banks::{McbConfig, TreasuryPrimeConfig, TrustBanksConfig},
    Account, PartnerAccounts, SecretStoreConfig, TrusteeConfig,
};
use url::Url;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

#[test]
fn basic_yaml_snapshot() {
    assert_yaml_snapshot!(TrusteeConfig {
        xand_api_endpoint: Url::parse("https://test.example.com/some-url").unwrap(),
        xand_api_jwt_secret_key: Some("fake-secret-key".into()),
        xand_api_timeout_seconds: Some(Duration::from_secs(25)),
        trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7".into(),
        access_token_folder: "some/path/to/folder".into(),
        polling_delay: 100,
        iterations_for_status_check: 200,
        redeem_retry_delay: 300,
        banks: HashSet::from_iter(
            [
                TrustBanksConfig::Mcb(McbConfig {
                    bank_api_url: Url::parse("https://test.example.com/some-other-url").unwrap(),
                    routing_number: "123456789".into(),
                    trust_account: "012345678".into(),
                    refresh_if_expiring_in_secs: Some(123),
                    secret_key_username: "username_key".into(),
                    secret_key_password: "password_key".into(),
                    secret_key_client_app_ident: "app_ident_key".into(),
                    secret_key_organization_id: "organization_id_key".into(),
                    display_name: "MCB Trust Account".into(),
                }),
                TrustBanksConfig::TreasuryPrime(TreasuryPrimeConfig {
                    bank_api_url: Url::parse("https://test.example.com/some-other-url").unwrap(),
                    secret_key_username: "username_key".into(),
                    secret_key_password: "password_key".into(),
                    routing_number: "123456789".into(),
                    trust_account: "012345678".into(),
                    display_name: "Provident Trust Account".into(),
                }),
                TrustBanksConfig::TreasuryPrime(TreasuryPrimeConfig {
                    bank_api_url: Url::parse("https://test.example.com/some-other-url").unwrap(),
                    secret_key_username: "username_key".into(),
                    secret_key_password: "password_key".into(),
                    routing_number: "123456789".into(),
                    trust_account: "012345678".into(),
                    display_name: "Test Bank 1 Trust Account".into(),
                }),
                TrustBanksConfig::TreasuryPrime(TreasuryPrimeConfig {
                    bank_api_url: Url::parse("https://test.example.com/some-other-url").unwrap(),
                    secret_key_username: "username_key".into(),
                    secret_key_password: "password_key".into(),
                    routing_number: "123456789".into(),
                    trust_account: "012345678".into(),
                    display_name: "Test Bank 2 Trust Account".into(),
                }),
            ]
            .iter()
            .cloned()
        ),
        partner_accounts: PartnerAccounts {
            members_to_accounts: ({
                let mut map = HashMap::new();
                map.insert(
                    "member_id".into(),
                    vec![Account {
                        account_number: "987654321".into(),
                        routing_number: "876543210".into(),
                    }],
                );
                map
            }),
            validators_to_accounts: ({
                let mut map = HashMap::new();
                map.insert(
                    "validator_id".into(),
                    vec![Account {
                        account_number: "765487623".into(),
                        routing_number: "876543210".into(),
                    }],
                );
                map
            }),
        },
        secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: "path/to/secrets.yaml".into()
        }),
        transaction_db_path: "path/to/db.sqlite3".into(),
        sync_policy: SynchronizationPolicyConfiguration {
            cooldown_timeout: Duration::from_secs(60),
            periodic_synchronization_interval: Duration::from_secs(3600),
        }
    });
}
