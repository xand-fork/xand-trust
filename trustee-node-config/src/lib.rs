mod errors;
pub mod trust_banks;

pub use crate::errors::TrustConfigValidationError;
pub use bank_transaction_repository::SynchronizationPolicyConfiguration;

use bank_transaction_repository::{
    BankConfiguration, BankTransactionRepositoryConfiguration, SecretStoreConfiguration,
    StoreConfiguration,
};
use trust_banks::TrustBanksConfig;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate derive_more;

pub use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

use itertools::Itertools;
use serde_with::{serde_as, DurationSeconds};
use std::collections::HashSet;
use std::time::Duration;
use std::{collections::HashMap, path::Path};
pub use url::Url;
use xand_banks::banks::config::BankConfig;

type Result<T, E = TrustConfigValidationError> = std::result::Result<T, E>;

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct TrusteeConfig {
    #[serde(with = "url_serde")]
    pub xand_api_endpoint: Url,
    pub xand_api_jwt_secret_key: Option<String>,
    #[serde(default)]
    #[serde_as(as = "Option<DurationSeconds>")]
    pub xand_api_timeout_seconds: Option<Duration>,
    /// Must be set to the address of the trust itself - used when making requests to xand-api
    /// to identify the trust's signing key
    pub trust_address: String,
    pub access_token_folder: String,
    pub polling_delay: u64,
    pub iterations_for_status_check: u64,
    pub redeem_retry_delay: u64,

    #[serde(serialize_with = "serde_ordered_collections::flat::sorted_serialize")]
    pub banks: HashSet<TrustBanksConfig>,
    /// If present, contains a mapping of partners to accounts that they consider valid for
    /// redemption of funds. All redemption requests must be checked against this allowlist before
    /// moving fiat.
    #[serde(default)]
    pub partner_accounts: PartnerAccounts,

    // source for bank credentials
    pub secret_store: SecretStoreConfig,

    // transactions store
    pub transaction_db_path: String,

    // Configuration containing cooldown and periodicity settings for the BankTransactionRepository
    pub sync_policy: SynchronizationPolicyConfiguration,
}

#[derive(Constructor, Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Account {
    pub account_number: String,
    pub routing_number: String,
}

/// Holds an allowlist of accounts that network participants have provided, so we have a double-check that
/// any given redemption is flowing to an authorized account
#[derive(Clone, Debug, Deserialize, Serialize, Default, Eq, PartialEq)]
pub struct PartnerAccounts {
    #[serde(serialize_with = "serde_ordered_collections::map::sorted_serialize")]
    pub members_to_accounts: HashMap<String, Vec<Account>>,
    #[serde(serialize_with = "serde_ordered_collections::map::sorted_serialize")]
    pub validators_to_accounts: HashMap<String, Vec<Account>>,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum SecretStoreConfig {
    LocalFile(LocalFileSecretStoreConfiguration),
}

impl TrusteeConfig {
    pub fn validate_trustee_config(&self) -> Result<()> {
        if !Path::new(&self.access_token_folder).exists() {
            return Err(TrustConfigValidationError::AccessTokenDirNotFound {
                path: self.access_token_folder.clone(),
            });
        }

        match &self.secret_store {
            SecretStoreConfig::LocalFile(config) => {
                if !Path::new(&config.yaml_file_path).exists() {
                    return Err(
                        TrustConfigValidationError::SecretStoreConfigurationInvalid {
                            message: format!(
                                "local secret file {:?} does not exist",
                                config.yaml_file_path
                            ),
                        },
                    );
                }
            }
        }

        if self.iterations_for_status_check < 1 {
            return Err(TrustConfigValidationError::BadConfigValue {
                msg: format!(
                    "Iteration for a status check must be greater than 0. '{}' was specified",
                    self.iterations_for_status_check
                ),
            });
        }

        self.validate_bank_configs()?;

        Ok(())
    }

    fn validate_bank_configs(&self) -> Result<(), TrustConfigValidationError> {
        let mut allbanks = vec![];
        for config in &self.banks {
            match config {
                TrustBanksConfig::Mcb(cfg) => {
                    let cfg: BankConfig = cfg.clone().into();
                    cfg.validate()?;
                    allbanks.push(cfg);
                }
                TrustBanksConfig::TreasuryPrime(cfg) => {
                    let cfg: BankConfig = cfg.clone().into();
                    cfg.validate()?;
                    allbanks.push(cfg);
                }
            }
        }

        for combo_vec in allbanks.iter().combinations(2) {
            // This is necessarily true
            if let [a, b] = combo_vec.as_slice() {
                if a.routing_number == b.routing_number {
                    return Err(TrustConfigValidationError::DuplicateRoutingNumber {
                        msg: a.routing_number.clone(),
                    });
                }
            }
        }

        Ok(())
    }
}
impl PartnerAccounts {
    /// Returns true if the provided account has been allowlisted for any member or validator
    pub fn is_allowlisted(&self, account: &Account) -> bool {
        let is_allowlisted_member = self
            .members_to_accounts
            .values()
            .flatten()
            .any(|ma| ma == account);

        let is_allowlisted_validator = self
            .validators_to_accounts
            .values()
            .flatten()
            .any(|va| va == account);

        is_allowlisted_member || is_allowlisted_validator
    }
}

impl Default for TrusteeConfig {
    fn default() -> Self {
        TrusteeConfig {
            xand_api_endpoint: Url::parse("http://validator").unwrap(),
            xand_api_jwt_secret_key: None,
            xand_api_timeout_seconds: None,
            access_token_folder: "".to_string(),
            trust_address: "".to_string(),
            polling_delay: 0,
            iterations_for_status_check: 0,
            redeem_retry_delay: 0,
            banks: HashSet::default(),
            partner_accounts: PartnerAccounts::default(),
            secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: "".into(),
            }),
            transaction_db_path: "".into(),
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Default::default(),
                periodic_synchronization_interval: Default::default(),
            },
        }
    }
}

impl From<TrusteeConfig> for BankTransactionRepositoryConfiguration {
    fn from(trustee_config: TrusteeConfig) -> Self {
        let banks =
            btr_banks_from_trustee_banks(&trustee_config.banks, &trustee_config.sync_policy);
        BankTransactionRepositoryConfiguration {
            banks,
            store: StoreConfiguration::Sqlite {
                path: trustee_config.transaction_db_path.clone(),
            },
            secret_store: match &trustee_config.secret_store {
                SecretStoreConfig::LocalFile(f) => SecretStoreConfiguration::LocalFile(f.clone()),
            },
        }
    }
}

fn btr_banks_from_trustee_banks(
    trust_banks_config: &HashSet<TrustBanksConfig>,
    sync_policy_config: &SynchronizationPolicyConfiguration,
) -> HashSet<BankConfiguration> {
    let mut banks = HashSet::new();
    for cfg in trust_banks_config {
        match cfg {
            TrustBanksConfig::Mcb(mcb) => {
                let bank = BankConfiguration {
                    routing_number: mcb.routing_number.clone(),
                    account_numbers: vec![mcb.trust_account.clone()].into_iter().collect(),
                    bank: mcb.clone().into(),
                    sync_policy: sync_policy_config.clone(),
                };
                banks.insert(bank);
            }
            TrustBanksConfig::TreasuryPrime(tp) => {
                let bank = BankConfiguration {
                    routing_number: tp.routing_number.clone(),
                    account_numbers: vec![tp.trust_account.clone()].into_iter().collect(),
                    bank: tp.clone().into(),
                    sync_policy: sync_policy_config.clone(),
                };
                banks.insert(bank);
            }
        }
    }
    banks
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use crate::{
        trust_banks::{McbConfig, TreasuryPrimeConfig, TrustBanksConfig},
        Account, PartnerAccounts, SecretStoreConfig, TrusteeConfig,
    };
    use assert_matches::assert_matches;
    use bank_transaction_repository::{
        BankClientConfiguration, BankConfiguration, BankTransactionRepositoryConfiguration,
        SecretStoreConfiguration, StoreConfiguration, SynchronizationPolicyConfiguration,
    };
    use insta::assert_yaml_snapshot;
    use std::collections::HashSet;
    use std::iter::FromIterator;
    use std::{collections::HashMap, path::PathBuf, time::Duration};
    use url::Url;
    use xand_banks::banks::{
        mcb_adapter::config::{McbClientTokenRetrievalAuthConfig, RawAccountInfoManagerConfig},
        treasury_prime_adapter::config::TreasuryPrimeAuthConfig,
    };
    use xand_banks::constants::BANK_CALL_STANDARD_TIMEOUT;
    use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

    fn create_valid_config() -> TrusteeConfig {
        TrusteeConfig {
            xand_api_endpoint: Url::parse("http://127.0.0.1:10044").unwrap(),
            xand_api_jwt_secret_key: None,
            xand_api_timeout_seconds: None,
            access_token_folder: "./".to_string(),
            trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7".to_string(),
            polling_delay: 5,
            iterations_for_status_check: 60,
            redeem_retry_delay: 300,
            banks: HashSet::default(),
            partner_accounts: PartnerAccounts::default(),
            secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                    .join("../config/secrets/secrets.yaml")
                    .to_string_lossy()
                    .to_string(),
            }),
            transaction_db_path: PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("../config/db/test_db.sqlite3")
                .to_string_lossy()
                .to_string(),
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Duration::from_secs(60),
                periodic_synchronization_interval: Duration::from_secs(3600),
            },
        }
    }

    #[test]
    fn validate_trustee_config__succeeds_for_valid_config() {
        let config = create_valid_config();

        config.validate_trustee_config().unwrap();
    }

    #[test]
    fn incorrect_iterations_for_status() {
        // Given
        let mut config = create_valid_config();
        config.iterations_for_status_check = 0;

        // When
        let err = config.validate_trustee_config();

        // Then
        let err = err.unwrap_err().to_string();
        assert_eq!(
            err,
            "Bad configuration value: Iteration for a status check must be greater than 0. \'0\' was specified"
        );
    }

    #[test]
    fn validate_trustee_config__access_token_dir_must_exist() {
        let trustee_config = create_valid_config();

        let mut bad_trustee_config = trustee_config;
        bad_trustee_config.access_token_folder = "/etc/some_nonexistent_path".to_string();

        let error = bad_trustee_config.validate_trustee_config().unwrap_err();
        assert_eq!(
            format!("{}", error),
            "Access token directory \"/etc/some_nonexistent_path\" does not exist"
        );
    }

    #[test]
    fn validate_trustee_config__secret_file_must_exist() {
        let trustee_config = create_valid_config();

        let mut bad_trustee_config = trustee_config;
        bad_trustee_config.secret_store =
            SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: "this_definitely_doesnt_exist.yaml".to_string(),
            });

        let error = bad_trustee_config.validate_trustee_config().unwrap_err();
        assert_eq!(format!("{}", error), "Invalid secret store configuration: local secret file \"this_definitely_doesnt_exist.yaml\" does not exist");
    }

    #[test]
    fn allowlist_returns_allowlisted_status_correctly() {
        let mut allowlist = PartnerAccounts::default();
        let fakeacct = Account::new("fakeacct".into(), "fakeroute".into());
        allowlist
            .members_to_accounts
            .insert("Partnername".into(), vec![fakeacct.clone()]);

        assert!(allowlist.is_allowlisted(&fakeacct));
        assert!(!allowlist.is_allowlisted(&Account {
            account_number: "".to_string(),
            routing_number: "".to_string()
        }))
    }

    #[test]
    fn member_allowlist_can_be_roundtrip_serialized() {
        let mut cfg = TrusteeConfig::default();
        let mut allowlist = PartnerAccounts::default();
        let fakeacct = Account::new("fakeacct".into(), "fakeroute".into());
        allowlist
            .members_to_accounts
            .insert("MemberPartnername".into(), vec![fakeacct.clone()]);
        cfg.partner_accounts = allowlist;
        let res = serde_yaml::to_string(&cfg).unwrap();
        let cfg2: TrusteeConfig = serde_yaml::from_str(&res).unwrap();
        assert_eq!(cfg, cfg2);
        assert!(cfg2.partner_accounts.is_allowlisted(&fakeacct))
    }

    #[test]
    fn validator_allowlist_can_be_roundtrip_serialized() {
        let mut cfg = TrusteeConfig::default();
        let mut allowlist = PartnerAccounts::default();
        let fakeacct = Account::new("fakeacct".into(), "fakeroute".into());
        allowlist
            .validators_to_accounts
            .insert("ValidatorName".into(), vec![fakeacct.clone()]);
        cfg.partner_accounts = allowlist;
        let res = serde_yaml::to_string(&cfg).unwrap();
        let cfg2: TrusteeConfig = serde_yaml::from_str(&res).unwrap();
        assert_eq!(cfg, cfg2);
        assert!(cfg2.partner_accounts.is_allowlisted(&fakeacct))
    }

    #[test]
    fn partner_accounts_yaml_to_string__produces_correctly_sorted_entries_consistently() {
        for _i in 0..100 {
            // Given
            let data = PartnerAccounts {
                members_to_accounts: {
                    let mut map = HashMap::new();
                    map.insert("d".into(), Vec::default());
                    map.insert("a".into(), Vec::default());
                    map.insert("e".into(), Vec::default());
                    map.insert("c".into(), Vec::default());
                    map.insert("b".into(), Vec::default());

                    map
                },
                validators_to_accounts: {
                    let mut map = HashMap::new();
                    map.insert("validator1".into(), Vec::default());
                    map.insert("validator0".into(), Vec::default());
                    map.insert("validator2".into(), Vec::default());

                    map
                },
            };

            // When
            let string = serde_yaml::to_string(&data).unwrap();

            // Then
            assert_eq!(
                string,
                r###"---
members_to_accounts:
  a: []
  b: []
  c: []
  d: []
  e: []
validators_to_accounts:
  validator0: []
  validator1: []
  validator2: []
"###
            );
        }
    }

    #[test]
    fn partner_accounts_yaml_to_string__multiple_accounts_per_validator_produces_accounts_in_order_added(
    ) {
        let fakeacct1 = Account::new("1".into(), "fakeroute123".into());
        let fakeacct2 = Account::new("2".into(), "fakeroute123".into());
        let fakeacct3 = Account::new("3".into(), "fakeroute123".into());

        // Given
        let data = PartnerAccounts {
            members_to_accounts: {
                let mut map = HashMap::new();
                map.insert("somemember".into(), Vec::default());

                map
            },
            validators_to_accounts: {
                let mut map = HashMap::new();
                map.insert(
                    "validator1".into(),
                    vec![fakeacct3, fakeacct1.clone(), fakeacct2],
                );
                map.insert("validator0".into(), vec![fakeacct1.clone()]);
                map.insert("validator2".into(), vec![fakeacct1]);

                map
            },
        };

        // Then
        assert_yaml_snapshot!(data, @r###"
        ---
        members_to_accounts:
          somemember: []
        validators_to_accounts:
          validator0:
            - account_number: "1"
              routing_number: fakeroute123
          validator1:
            - account_number: "3"
              routing_number: fakeroute123
            - account_number: "1"
              routing_number: fakeroute123
            - account_number: "2"
              routing_number: fakeroute123
          validator2:
            - account_number: "1"
              routing_number: fakeroute123
        "###);
    }

    #[test]
    fn btr_config_from__converts_trustee_as_expected() {
        // Given
        let expected_secret_store_config = LocalFileSecretStoreConfiguration {
            yaml_file_path: PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("../config/secrets/secrets.yaml")
                .to_string_lossy()
                .to_string(),
        };

        let transaction_db_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("../config/db/test_db.sqlite3")
            .to_string_lossy()
            .to_string();

        let trustee_config = TrusteeConfig {
            xand_api_endpoint: Url::parse("http://127.0.0.1:10044").unwrap(),
            xand_api_jwt_secret_key: None,
            xand_api_timeout_seconds: None,
            access_token_folder: "./".to_string(),
            trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7".to_string(),
            polling_delay: 5,
            iterations_for_status_check: 60,
            redeem_retry_delay: 300,
            banks: HashSet::from_iter(
                [
                    TrustBanksConfig::Mcb(McbConfig {
                        bank_api_url: Url::parse("http://mcb.com").unwrap(),
                        routing_number: "101010".to_string(),
                        trust_account: "232323".to_string(),
                        refresh_if_expiring_in_secs: None,
                        secret_key_username: "mcb username key".to_string(),
                        secret_key_password: "mcb password key".to_string(),
                        secret_key_client_app_ident: "mcb client app ident key".to_string(),
                        secret_key_organization_id: "mcb organization id key".to_string(),
                        display_name: "mcb".to_string(),
                    }),
                    TrustBanksConfig::TreasuryPrime(TreasuryPrimeConfig {
                        bank_api_url: Url::parse("http://treasury-prime.com").unwrap(),
                        routing_number: "454545".to_string(),
                        trust_account: "676767".to_string(),
                        secret_key_username: "treasury prime username key".to_string(),
                        secret_key_password: "treasury prime password key".to_string(),
                        display_name: "treasury prime".to_string(),
                    }),
                ]
                .iter()
                .cloned(),
            ),
            partner_accounts: PartnerAccounts::default(),
            secret_store: SecretStoreConfig::LocalFile(expected_secret_store_config.clone()),
            transaction_db_path: transaction_db_path.clone(),
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Default::default(),
                periodic_synchronization_interval: Default::default(),
            },
        };

        let expected_btr_config = BankTransactionRepositoryConfiguration {
            banks: vec![
                BankConfiguration {
                    routing_number: "101010".to_string(),
                    account_numbers: vec!["232323".to_string()].into_iter().collect(),
                    bank: BankClientConfiguration::Mcb(
                        xand_banks::banks::mcb_adapter::config::McbConfig {
                            url: Url::parse("http://mcb.com").unwrap(),
                            refresh_if_expiring_in_secs: None,
                            auth: McbClientTokenRetrievalAuthConfig {
                                secret_key_username: "mcb username key".to_string(),
                                secret_key_password: "mcb password key".to_string(),
                            },
                            account_info_manager: RawAccountInfoManagerConfig {
                                secret_key_client_app_ident: "mcb client app ident key".to_string(),
                                secret_key_organization_id: "mcb organization id key".to_string(),
                            },
                            timeout: BANK_CALL_STANDARD_TIMEOUT,
                        },
                    ),
                    sync_policy: SynchronizationPolicyConfiguration {
                        cooldown_timeout: Default::default(),
                        periodic_synchronization_interval: Default::default(),
                    },
                },
                BankConfiguration {
                    routing_number: "454545".to_string(),
                    account_numbers: vec!["676767".to_string()].into_iter().collect(),
                    bank: BankClientConfiguration::Provident(
                        xand_banks::banks::treasury_prime_adapter::config::TreasuryPrimeConfig {
                            url: Url::parse("http://treasury-prime.com").unwrap(),
                            auth: TreasuryPrimeAuthConfig {
                                secret_key_username: "treasury prime username key".to_string(),
                                secret_key_password: "treasury prime password key".to_string(),
                            },
                            timeout: BANK_CALL_STANDARD_TIMEOUT,
                        },
                    ),
                    sync_policy: SynchronizationPolicyConfiguration {
                        cooldown_timeout: Default::default(),
                        periodic_synchronization_interval: Default::default(),
                    },
                },
            ]
            .into_iter()
            .collect(),
            store: StoreConfiguration::Sqlite {
                path: transaction_db_path,
            },
            secret_store: SecretStoreConfiguration::LocalFile(expected_secret_store_config.clone()),
        };

        // When
        let btr_config = BankTransactionRepositoryConfiguration::from(trustee_config);

        // Then
        assert_eq!(btr_config.banks, expected_btr_config.banks);
        assert_eq!(btr_config.store, expected_btr_config.store);
        assert_matches!(
            btr_config.secret_store,
            SecretStoreConfiguration::LocalFile(config) if config == expected_secret_store_config
        );
    }
}
