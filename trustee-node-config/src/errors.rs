use snafu::Snafu;
use xand_banks::errors::XandBanksErrors;

#[derive(Debug, Snafu, Clone, Serialize)]
pub enum TrustConfigValidationError {
    #[snafu(display("Access token directory {:?} does not exist", path))]
    AccessTokenDirNotFound { path: String },
    #[snafu(display("Invalid secret store configuration: {}", message))]
    SecretStoreConfigurationInvalid { message: String },
    #[snafu(display("Bad configuration value: {}", msg))]
    BadConfigValue { msg: String },
    #[snafu(display("Problem with bank: {}", source))]
    XandBankError { source: XandBanksErrors },
    #[snafu(display(
        "Two different bank configurations have the same routing number: {}",
        msg
    ))]
    DuplicateRoutingNumber { msg: String },
}

impl From<XandBanksErrors> for TrustConfigValidationError {
    fn from(e: XandBanksErrors) -> Self {
        TrustConfigValidationError::XandBankError { source: e }
    }
}
