# Trust Software Threat Self-Assessment

The trust software runs as a daemon in a docker container called `xand-trust`.

## Ingresses

None.

## Egresses

| System | Function|  Protocol / Version | Port| Auth | Scope |
| -- | -- | -- | -- | -- | -- |
| MCB | Bank API | HTTPS | 443 | MCB Auth | Internet |
| Provident | Bank API | HTTPS | 443 | Treasury Prime Auth | Internet |
| Xand API | Interface to the Validator | gRPC | 10044 | JWT | Internal Network |

## Secrets

| Secret | Usage | Storage | Encrypted at Rest | Encrypted in Transit | Notes |
| -- | -- | -- | -- | -- | -- |
| Private Key | Sign Transactions | Kubernetes Secret | yes | yes | This is controlled by the installer's Kubernetes configuration. |
| JWT Token | Auth to the Xand API | Kubernetes Secret | yes | yes | This is controlled by the installer's Kubernetes configuration. |
| MCB Credentials | username, password, client_app_ident, and organization_id are values that MCB requires to auth to their API during bank transfer verifications. | Kubernetes Secret | yes | yes |  |
| Provident Credentials | api_key_id and api_key_value are values that Provident requires to auth to their API during bank transfer verifications. | Kubernetes Secret | yes | yes | |