#!/bin/bash

# This is a script that determines how to run the trust-compiler docker container based on whether the integration tests
# are being run from within CI or locally.

COMPILER_CONTAINER_NAME=$1
COMPILER_IMAGE_TAG=$2

if [ -n "$DEPLOY_SSH_KEY" ]; then
  # We are running the tests in CI and must authenticate SSH with the CI environment variable
  docker run --rm --name $COMPILER_CONTAINER_NAME -e CI_SSH_KEY="$(cat $DEPLOY_SSH_KEY)" --volume $(pwd)/../..:/xand-trust $COMPILER_IMAGE_TAG /xand-trust/.docker/integ-test/trust-compiler-build.sh
else
  # We are running the tests locally and must mount the dev's ssh information (assumes key is held in a standard ssh file with a standard file path)
  docker run --rm --name $COMPILER_CONTAINER_NAME --volume $(pwd)/../..:/xand-trust --volume $HOME/.ssh:/root/.ssh $COMPILER_IMAGE_TAG /xand-trust/.docker/integ-test/trust-compiler-build.sh
fi
