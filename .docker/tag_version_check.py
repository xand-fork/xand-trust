#!/usr/bin/env python3
import argparse
import sys
from semantic_version import Version
from semantic_version import SimpleSpec


def strip_v_prefix(tag):
    if tag.startswith("v"):
        return tag[1:]
    else:
        return tag


# This will check if the old_tag is higher than the new tag.
# It will only compare them if they are release tags (e.g. -beta tags are ignored).
def tag_is_newest_version(proposed_tag, old_tag):
    # split tags into main semver components
    old_tag = Version(old_tag)
    proposed_tag = Version(proposed_tag)
    is_pre_release = proposed_tag.prerelease != ()

    return proposed_tag > old_tag or is_pre_release

def run(new_tag, existing_tags_file_path):
    with open(existing_tags_file_path) as tag_file:
        existing_tags = tag_file.read().splitlines()

    # exclude the current tag from the comparison set
    if new_tag in existing_tags:
        existing_tags.remove(new_tag)
    # strip 'v' prefixes
    existing_tags = list(map(strip_v_prefix, existing_tags))


    conflicting_tags = list(filter(lambda x: not tag_is_newest_version(strip_v_prefix(new_tag), x), existing_tags))

    if len(conflicting_tags) > 0:
        print("conflicting tags: " + str(conflicting_tags))
        return False

    return True



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Check if new tag is properly incremented.')
    parser.add_argument('new_tag', type=str, help='The proposed tag to check')
    parser.add_argument('existing_tags', type=str, help='file containing existing tags')

    args = parser.parse_args()

    run(args.new_tag, args.existing_tags)





