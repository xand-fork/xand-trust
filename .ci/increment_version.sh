#!/bin/bash

# Increment a version string using Semantic Versioning (SemVer) terminology.

# Parse command line options.

while getopts ":MmpP" Option
do
  case $Option in
    M ) major=true;;
    m ) minor=true;;
    p ) patch=true;;
    P ) patch=true
        prerelease=true
        PRERELEASE_NAME=$2
        shift $(($OPTIND - 1))
        ;;
  esac
done

shift $(($OPTIND - 1))

version=$1

# Build array from version string.

a=( ${version//./ } )

# Increment version numbers as requested.

if [ ! -z $major ]
then
  ((a[0]++))
  a[1]=0
  a[2]=0
fi

if [ ! -z $minor ]
then
  ((a[1]++))
  a[2]=0
fi

if [ ! -z $patch ]
then
  ((a[2]++))
fi

if [ ! -z $prerelease ]
then
  NEW_VERSION="${a[0]}.${a[1]}.${a[2]}-$PRERELEASE_NAME"
else
  NEW_VERSION="${a[0]}.${a[1]}.${a[2]}"
fi

echo $NEW_VERSION
